GCC=gcc
CLANG=clang
CPP=g++
CC=cc
CFLAGS = -lm -lncurses -lpanel -lmenu
DEBUG = -g
STD99 =  -std=c99 
SOURCES = ./src/functions.c ./src/game.c ./src/heap.c ./src/lib.c ./src/parser.c ./src/pathfind.c ./src/set.c ./src/knight.c

all: main.c $(SOURCES)
	$(CLANG) -o thegame main.c $(SOURCES) $(CFLAGS)

gcc: main.c $(SOURCES)
	$(GCC) -o thegame main.c $(SOURCES) $(CFLAGS)

g++: main.c $(SOURCES)
	$(CPP) -o thegame main.c $(SOURCES) $(CFLAGS)

clang: main.c $(SOURCES)
	$(CLANG) -o thegame main.c $(SOURCES) $(CFLAGS)

wextra: main.c $(SOURCES)
	$(GCC) -o thegame main.c $(SOURCES) $(CFLAGS) -Wextra -Wstrict-prototypes -Wconversion -Wunreachable-code -Wall -Wuninitialized

debug: main.c $(SOURCES)
	$(GCC) -o thegame main.c $(SOURCES) $(CFLAGS) $(DEBUG)

refresh:
	find ./ -type f -exec touch {} +
