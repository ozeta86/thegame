#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include "./headers/functions.h"
#include "./headers/lib.h"

#define NORTH(y) y - 1
#define SOUTH(y) y + 1
#define EAST(y) y + 1
#define WEST(y) y - 1

/**
 * stampa un messaggio di avviso su standard output
 * @param message stringa messaggio
 */
void dialogPanel ( char *message ) {
	printf ( "%s", message );
	printf ("\n");
}


/**
 * alloca ed inizializza un oggetto grafo
 * @param build
 * @param insertEdge
 * @param getWeight
 * @param insertVertex
 * @param transpose
 * @param getAdjList
 * @param path
 * @return 
 */
GRAPHOBJ *initializeGraph ( BDEF build, IEDEF insertEdge, WDEF getWeight, 
	VINS insertVertex, TDEF transpose, ADEF getAdjList, PATHD path ) {

	GRAPHOBJ * 	graph = ( GRAPHOBJ * ) malloc ( sizeof ( GRAPHOBJ ) );

	if ( check ( graph, "initializeGraph" ) ) {

		graph->maze 			= NULL;
		graph->vNum 			= 0;
		graph->width 			= 0;
		graph->height 			= 0;
		graph->build 			= build;
		graph->insertEdge 		= insertEdge;
		graph->getWeight 		= getWeight;
		graph->insertVertex 	= insertVertex;
		graph->transpose 		= transpose;
		graph->getAdjList		= getAdjList;
		graph->path				= path;
	}

	return graph;
}

GraphT matrixEdgeInizialize () {
	GraphT edge;
	edge.weight = 0;
	edge.ID 	= -1;
	edge.k 		= -1;	
	return edge;
}
/**
//input: numero di vertici da allocare
//return: nuovo puntatore a grafo di vertexNum vertici
*/
GRAPHOBJ *buildGraph ( GRAPHOBJ *graphObj, int size ) {
	assert ( graphObj );	
	int i, j;
	char msg[256];
	GraphT **graph = NULL; 
	graph = ( GraphT ** ) malloc ( ( size ) * sizeof ( GraphT * ) );
	if (graph == NULL) {
		dialogPanel ("memoria insufficiente: graph"), exit(EXIT_FAILURE);
	}
	for ( i = 0; i < size; i++) {
		graph[i] = (GraphT *) malloc (size * sizeof (GraphT));
		if (graph[i] == NULL){
			sprintf ( msg, "memoria insufficiente: graph[%d]", i );
			dialogPanel ( msg ), exit(EXIT_FAILURE);
		}
		for ( j = 0; j < size; j++) {
		graph[i][j] = matrixEdgeInizialize ();
		}
	} 

	graphObj->matrix = graph;
	
	return graphObj;
}

void matrixInsertInterface ( GRAPHOBJ *graph ) {
	assert ( graph );
	graph->matrix = appendOneVertexIntoGraph ( graph );
}
/**
//input: numero corrente di vertici, puntatore ad graph
//return: nuovo puntatore ad graph di dimensione n+1
*/
GraphT **appendOneVertexIntoGraph ( GRAPHOBJ *graph ) {
	assert ( graph );
	//aggiorno il numero di vertici
	++graph->vNum;
	char msg[256];
 	int i = 0;

	//rialloco l'array di puntatori aggiungendo il posto per
	//il nuovo vertice
	graph->matrix = ( GraphT ** ) realloc ( graph->matrix, ( graph->vNum ) * sizeof ( GraphT * ) );
	if (graph->matrix == NULL)
		dialogPanel ( ">realloc: memoria insufficiente" ), exit ( EXIT_FAILURE );
	//alloco memoria per il nuovo array di archi
	graph->matrix[graph->vNum-1] = ( GraphT * ) malloc (graph->vNum  * sizeof ( GraphT ) );
	if  ( graph->matrix[graph->vNum-1] == NULL ) {
		sprintf ( msg, ">realloc: memoria insufficiente: graph->matrix[%d]", graph->vNum -1 );
			dialogPanel  ( msg ), exit ( EXIT_FAILURE );
	}
	//rialloco memoria per tutti gli array di archi ad eccezione di "vertexnum-1"
	for  (  i = 0; i < graph->vNum; i++ ) {
		if  ( i != graph->vNum-1 ) {
			graph->matrix[i] = ( GraphT * ) realloc  ( graph->matrix[i], ( graph->vNum ) * sizeof  ( GraphT ) );
			if  ( graph->matrix[i] == NULL ){
				sprintf ( msg, ">realloc: memoria insufficiente: graph->matrix[%d]", i );
				dialogPanel  ( msg ), exit ( EXIT_FAILURE );
			}
		}
		//inizializzo il nuovo array di archi e i nuovi archi per ogni nodo
		graph->matrix[graph->vNum-1][i] = matrixEdgeInizialize ();
		graph->matrix[i][graph->vNum-1] = matrixEdgeInizialize ();
	}

    return graph->matrix;
}
/*estrae il peso da un arco*/
int getMatrixWeight ( GRAPHOBJ *graph, int x, int y ) {
	assert ( graph );	
	int weight = 0;

	if ( graph && graph->matrix ) {
		weight = graph->matrix[x][y].weight;
	}
	return weight;
}

//procedura che modifica il peso di un arco
void insertEdgeIntograph ( GRAPHOBJ *graph, int x, int y, int weight ) {
	assert ( graph );	
	if ( graph ) {
			if ( x < graph->vNum && y < graph->vNum ) {
				graph->matrix[x][y].weight = weight;
			} else {
				printf ("coordinata errata\n");
			}
		} else {
		dialogPanel ("grafo non esistente\n");
		}
}

//elimina arco nel grafo
void deleteEdgeFromgraph ( GRAPHOBJ *graph, int x, int y ) {
	assert ( graph );	
	if ( graph ) {
		if ( x < graph->vNum && y < graph->vNum )
			graph->matrix[x][y].weight = 0;
	} else {
		dialogPanel ("grafo non esistente");
	}
}
void FillGraph ( GRAPHOBJ *graph, insDEF insertMethod, int inf, int sup) {
	assert ( graph );
	assert ( insertMethod );	
	if ( !check ( graph , "FillGraph" ) ) {
		return;
	}	
	srand (time(NULL));
	int i, j;
	for ( i = 0; i < graph->vNum; i++ ) {
		for ( j = 0; j < graph->vNum; j++ ) {
			graph->insertEdge ( graph, i, j, insertMethod (inf, sup) );
		}
	}
}


/**********************************
*IMPLICIT GRID MAZE SECTION
************************************/
VCOORD *getCoord ( GRAPHOBJ *graph, int u ) {
	assert ( graph );	
	VCOORD *vCoord = ( VCOORD * ) malloc ( sizeof ( VCOORD ) );
	int x = u % graph->width;
	int y = u / graph->width;
	vCoord->x = x;
	vCoord->y = y;

	return vCoord;
}

int coordToID ( GRAPHOBJ *graph, VCOORD *c ) {
	int res = ( graph->width ) * ( c->y ) + ( c->x );
	//printf ( "graph->width:%4d, c->y:%4d, c->x:%4d, res:%4d\n", graph->width, c->y, c->x, res );
	return res;
}

/**
stampa del cammino minimo
*/
/**
Set *printPath ( GRAPHOBJ *graph, int s , int v, int *pred, Set *succ, FILE *stream ) {
	assert ( graph );
    if (v == s) {
 		if ( stream )
 			fprintf ( stream, "s: %d\n", s  );
 		graph->maze[getCoord ( graph, s )->y][getCoord ( graph, s )->x].path = true;
 		succ = enqueue ( succ, setInt ( s ) );
    } else if ( pred[v] == NIL ) {
    	if ( stream )
			fprintf ( stream, "\n--non esiste cammino tra s e v--\n");
    } else {
    	if ( stream )
			fprintf ( stream, "v: %d\n", v );
		succ = enqueue ( succ, setInt ( v ) );

		graph->maze[getCoord ( graph, v )->y][getCoord ( graph, v )->x].path = true;
		printPath (graph, s, pred[v], pred, succ, stream );
    }
    return succ;
}
*/
Set *printPath ( GRAPHOBJ *graph, int s , int v, int *pred, Set *succ, FILE *stream ) {
	assert ( graph );
	if (v == s) {
		if ( stream )
			fprintf ( stream, "s: %d\n", s  );
		VCOORD *s_coord = getCoord ( graph, s );
		succ = enqueue ( succ, s_coord );
	} else if ( pred[v] == NIL ) {
		if ( stream )
			fprintf ( stream, "\n--non esiste cammino tra s e v--\n");

	} else {
		if ( stream )
			fprintf ( stream, "v: %d\n", v );
		VCOORD *v_coord = getCoord ( graph, v );
		succ = enqueue ( succ, v_coord );
		printPath (graph, s, pred[v], pred, succ, stream );
	}
	return succ;
}

Set *explicitPrintPath ( GRAPHOBJ *graph, int s , int v, int *pred, Set *succ, FILE *stream ) {
	assert ( graph );
    if (v == s) {
 		if ( stream )
 			fprintf ( stream, "s: %d\n", s  );
 		succ = enqueue ( succ, setInt ( s ) );
    } else if ( pred[v] == NIL ) {
    	if ( stream )
			fprintf ( stream, "\n--non esiste cammino tra s e v--\n");
    } else {
    	if ( stream )
			fprintf ( stream, "v: %d\n", v );
		succ = enqueue ( succ, setInt ( v ) );
		explicitPrintPath (graph, s, pred[v], pred, succ, stream );
    }
    return succ;
}

Set *minPath ( GRAPHOBJ *graph, int s, int v ) {
	assert ( graph );
	int 		*	pred 	= NULL;
	Set 		*	succ 	= NULL;

	pred = graph->path ( graph, s, v );
	if ( pred != NULL ) {

		FILE *stream = NULL;

		succ = printPath ( graph, s, v, pred, succ, stream );

	} else { 
		//printf ("\n--target not found--\n");
	}

	free ( pred );
	return succ;
}

Set *getAdjList ( GRAPHOBJ *graph, int u ) {
	assert ( graph );
	Set *res = NULL;
	int weight;
	int i;
	for (i = 0; i < graph->vNum ; i++) {
		weight = graph->getWeight ( graph, u, i );
		if ( weight != 0 && weight != INT_MIN ) {
			res = enqueue (res, setInt ( i ) );
		}
	}
	return res;
}

VCOORD **buildMap ( GRAPHOBJ * graph, char *mazeStr ) {
	assert ( graph );
	assert ( mazeStr );	
	int width 		= graph->width;
	int height 		= graph->height;
	int size 		= width * height;

	VCOORD **maze 	= ( VCOORD ** ) malloc ( height * sizeof ( VCOORD * ) );
	if ( maze == NULL ) {
		printf ("errore maze **\n");
		exit ( -1 );
	}
	int i;
	for ( i = 0; i < height; i++ ) {
		maze[i] = ( VCOORD * ) malloc ( width * sizeof ( VCOORD ) );
		if ( maze[i] == NULL ) {
			printf ( "width %d\n", graph->width );
			printf ( "errore maze * %d\n", i );
			exit ( -1 );
		}
	}
	int row;
	int col;
	int k = 0;
	i = 0;
	while ( i < size ) {
		row = i % width;
		col = i / width;
		if ( mazeStr[i] == '|' ) {
			k = 9;
		} else if ( mazeStr[i] == '-' ) {
			k = 9;
		} else if ( mazeStr[i] == '+' ) {
			k = 9;
		} else if ( mazeStr[i] == ' ' ) {
			k = 1;
		} else if ( mazeStr[i] == 's' ) {
			k = 2;
		} else if ( mazeStr[i] == 't' ) {
			k = 3;
		} else if ( mazeStr[i] == 'f' ) {
			k = 3;
		} else if ( mazeStr[i] == 'k' ) {
			k = 4;
		} else if ( mazeStr[i] == 'm' ) {
			k = 5;
		} else if ( mazeStr[i] == 'b' ) {
			k = 6;
		}

		maze[col][row].k = k;
		maze[col][row].ID = i;
		maze[col][row].path = false;
		
		i++;
		++graph->vNum;
	}
	free ( mazeStr );
	return maze;
}

