#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include "./headers/functions.h"
#include "./headers/set.h"
#include "./headers/lib.h"
#include "./headers/heap.h"
#include "./headers/pathfind.h"
#include "./headers/structures.h"
#include "headers/structures.h"

#define NORTH(y) y - 1
#define SOUTH(y) y + 1
#define EAST(y) y + 1
#define WEST(y) y - 1

bool PRINTALL = false;

//restituisce il peso di un arco nella mappa implicita.
//in caso di ostacolo (trappole), restituisce un valore arbitrariamente alto.

int getMazeWeight ( GRAPHOBJ *graph, int u, int v ) {
	VCOORD *vv = getCoord ( graph, v );
	int weight = graph->maze[vv->y][vv->x].k;
	if ( weight > 1 ) {
		weight = 30;
	}
	free ( vv );
	return weight;
}

//restituisce l'insieme dei vertici adiacenti sulla mappa implicita
//sono state create 4 direzioni principali ( N S E W ) e lasciate 
//su una mappa toroidale ( flag toroid )
Set *getMazeAdjList ( struct gObj *graph, int u ) {
	assert ( graph );
	Set *Adj = NULL;
	int x 			= u % graph->width;
	int y 			= u / graph->width;
	bool toroid 	= true;
	int neighbor;

	if ( toroid && NORTH(y) == 0 && graph->maze[NORTH(y)][x].k != 9 ) {
		neighbor = graph->width * (  0 ) + ( x );
		Adj = enqueue ( Adj, setInt ( neighbor ) );		
	}
	if ( toroid && NORTH(y) < 0 && graph->maze[graph->height -1][x].k != 9 ) {
		neighbor = graph->width * (  graph->height -1 ) + ( x );
		Adj = enqueue ( Adj, setInt ( neighbor ) );		
	}	
	if ( NORTH(y) > 0 && graph->maze[NORTH(y)][x].k != 9 ) {
	//if ( NORTH(y) > 0 ) {
		neighbor = graph->width * ( NORTH(y) ) + ( x );
		Adj = enqueue ( Adj, setInt ( neighbor ) );
	}

	if ( toroid && EAST(x) == graph->width - 1 && graph->maze[y][EAST(x)].k != 9 && graph->maze[y][0].k != 9) {
		neighbor = graph->width * ( y ) + ( EAST(x) );		
		Adj = enqueue ( Adj, setInt ( neighbor ) );
	}
	if ( toroid && EAST(x) > graph->width - 1 && graph->maze[y][0].k != 9 ) {
		neighbor = graph->width * ( y ) + ( 0 );		
		Adj = enqueue ( Adj, setInt ( neighbor ) );
	}	
	if ( EAST(x) < graph->width && graph->maze[y][EAST(x)].k != 9   ) {
	//if ( EAST(x) < graph->width  ) {
		neighbor = graph->width * ( y ) + ( EAST(x) );		
		Adj = enqueue ( Adj, setInt ( neighbor ) );
	}
	
	if ( toroid && SOUTH(y) == graph->height -1  && graph->maze[SOUTH(y)][x].k != 9 ) {
		neighbor = graph->width * (  SOUTH(y) ) + ( x );
		Adj = enqueue ( Adj, setInt ( neighbor ) );		
	}	
	if ( toroid && SOUTH(y) > graph->height -1  && graph->maze[(y)][x].k != 9&& graph->maze[0][x].k != 9 ) {
		neighbor = graph->width * (  0 ) + ( x );
		Adj = enqueue ( Adj, setInt ( neighbor ) );		
	}
	if ( SOUTH(y) < graph->height && graph->maze[SOUTH(y)][x].k != 9 ) {
	//if ( SOUTH(y) < graph->height ) {
		neighbor = graph->width * ( SOUTH(y) ) + ( x );		
		Adj = enqueue ( Adj, setInt ( neighbor ) );
	}
	
	if ( toroid && WEST(x) == 0 && graph->maze[y][WEST(x)].k != 9 ) {
		neighbor = graph->width * ( y ) + ( WEST(x) );		
		Adj = enqueue ( Adj, setInt ( neighbor ) );
	}

	if ( toroid && WEST(x) < 0 && graph->maze[y][(x)].k != 9 && graph->maze[y][graph->width - 1].k != 9) {
		neighbor = graph->width * ( y ) + ( graph->width - 1 );		
		Adj = enqueue ( Adj, setInt ( neighbor ) );
	}

	if ( WEST(x) > 0 && graph->maze[y][WEST(x)].k != 9 ) {
	//if ( WEST(x) > 0 ) {
		neighbor = graph->width * ( y ) + ( WEST(x) );		
		Adj = enqueue ( Adj, setInt ( neighbor ) );
	}

	return Adj;
}
/**
 * visita breadth_first_search su matrice di adiacenza
*/
int *dijkstraHeap ( GRAPHOBJ *graph, int s, int target ) {
	assert ( graph );	
	/**
	 * inizializzazioni
	 */
	Heap 	*		frontier 	= NULL;
	Set 	*		AdjList 	= NULL;
	Data 	*		tmp;
	int 	*		pred;

	frontier = initializeHeap ( minHeapify );
	int i 	= 0;
	int v 	= 0;
	int u 	= 0;
	int alt = 0;
	pred 	= ( int *) malloc ( graph->vNum * sizeof ( int ) );
	int colore[graph->vNum];
	int dist[graph->vNum];

	for ( v = 0; v < graph->vNum; v++ ) {
		dist[v] 	= INFINITE;
		pred[v] 	= NIL;
		colore[v] 	= WHITE;
	}
	dist[s] = 0;
	pred[s] = s;
	insert ( frontier, new_HeapData ( s, 0 ) );

	char path[50];
	buildHeap ( frontier );

	//main loop
	while ( frontier->heapsize > 1 ) {
		//estrazione del nodo con la distanza minore
		tmp 	= extractFirst ( frontier );
		u 		= getData ( tmp );
		dist[u] = getKey ( tmp );
		colore[u] = BLACK;
		AdjList = graph->getAdjList ( graph, u );

		if ( u == target ) {
		 	freeHeap ( frontier );
			return pred;			
		}
		//analisi dei nodi adiacenti
		while ( !isEmpty ( AdjList ) ) {
			v = getInt ( getFront ( AdjList ) );
			
			if ( colore[v] == WHITE ) {
				relax ( graph, frontier, u, v, dist, pred );
			}
			AdjList = dequeue ( AdjList );
		}
	}
	free ( pred );
 	freeHeap ( frontier );
	return NULL;
}
//funzione di rilassamento arco
void relax ( GRAPHOBJ *graph, Heap *frontier, int u, int v, int *dist, int *pred) {
	int alt;
	alt = dist[u] + graph->getWeight ( graph, u, v );
	if ( alt < dist[v] ) {
		dist[v] = alt;
		pred[v] = u;
		if ( heapIntSearch ( frontier , v ) ) {
			decreaseKey ( frontier, v, alt ) ;
		} else {
			insert ( frontier, new_HeapData ( v, alt ) );
		}
	}
}

//BFS
int *breadth_first_search ( GRAPHOBJ *graph, int s, int target ) {

	assert ( graph );

	int u;							//start node
	int v;							//target
	int color[graph->vNum];			//color of the node
	Set *	frontier	= NULL;		//frontier implemented as queue
	Set *	AdjList 	= NULL;		//Adjiacency list implemented as queue
	int *	pred		= ( int *) malloc ( graph->vNum * sizeof ( int ) );	//reverse path
	memset ( pred, 0, graph->vNum );

	//initialization
	for ( u = 0; u < graph->vNum; u++ ) {
		color[u] 	= WHITE;
		pred[u]		= NIL;
	}
	color[s] 	= GREY;
	pred[s]		= 0;

	frontier = enqueue ( frontier, setInt ( s ) );

	//estrazione nodo dalla frontiera
	while ( !isEmpty ( frontier ) ) {
		u = getInt ( getFront ( frontier ) );
		AdjList = graph->getAdjList ( graph, u );

		//inserimento degli adiacenti nella frontiera
		while ( !isEmpty ( AdjList ) ) {
			v = getInt ( getFront ( AdjList ) );
			AdjList = dequeue ( AdjList );
			if ( color[v] == WHITE ) {
				color[v] = GREY;
				pred[v] = u;
				frontier = enqueue ( frontier, setInt ( v ) );
			}
			if ( v == target ) {

				return pred;
			}
		}
		frontier = dequeue ( frontier );
		color[u] = BLACK;
	}
	free ( pred );
	return NULL;
}

//funzione euristica per A*
int heuristic ( GRAPHOBJ *graph, int s, int t ) {
	assert ( graph );
	int res;
	VCOORD *	start 		= getCoord ( graph, s );
	VCOORD *	target 		= getCoord ( graph, t );
	/*manhattan*/
	//res = ( abs ( start->x - target->x ) + abs ( start->y - target->y ) );
	/*euclidian*/
	res = ( int ) pow ( pow ( ( target->x - start->x ), 2 ) + pow ( ( target->y - start->y ), 2 ), 0.5 );
	free ( start );
	free ( target );
	return res;
}

//A*
int *a_star ( GRAPHOBJ *graph, int s, int t ) {
	assert ( graph );
	int i, cost;
	int current;

	void 	*	tmp;
	Set 	*	AdjList		= NULL;		//lista di adiacenza ( coda )
	Heap 	*	frontier	= NULL;		//Open set ( Heap )
	frontier 				= initializeHeap ( minHeapify );
	int 	*	pred		= ( int *) malloc ( graph->vNum * sizeof ( int ) );
	int 	*	g			= ( int *) malloc ( graph->vNum * sizeof ( int ) );


	//initialization
	for ( i = 0; i < graph->vNum; i++ ) {
		pred[i] 	= NIL;
		g[i] 	= INFINITE;
	}
	insert ( frontier, new_HeapData ( s, 0 ) );
	g[s] 	= 0;
	//estrazione del nodo con priorità minore dalla frontiera
	while ( !isHeapEmpty ( frontier ) ) {
		tmp 	= extractFirst ( frontier );
		current = getData ( ( Data * ) tmp );
		free ( tmp );
		if ( current == t ) {
			free ( g );
			freeHeap ( frontier );		
			return pred;
		}
		AdjList = graph->getAdjList ( graph, current );
		//estrazione da lista di adiacenza
		while ( !isEmpty ( AdjList ) ) {
			int y = getInt ( getFront ( AdjList ) );
			AdjList = dequeue ( AdjList );
			cost = g[current] + graph->getWeight ( graph, current, y );
			if ( g[y] == INFINITE || cost < g[y] ) {
				g[y] = cost;
				int priority = cost + heuristic ( graph, y, t );
				insert ( frontier, new_HeapData ( y, priority ) );
				pred[y] = current;
			}
		}

 	}
 	free ( pred );
 	free ( g );
 	freeHeap ( frontier );
	return NULL;
}
/**
ricerca percorso minimo
*/
Set *pathfind ( GRAPHOBJ *graph, VCOORD *target, VCOORD *start, PATHD alg ) {
	//alg: chiamata a funzione di ricerca percorso minimo ( A*, Dijkstra, BFS )
	graph->path = alg;
	//ricerca del percorso minimo. restituisce una coda di coordinate
	Set *succ = minPath ( graph, coordToID ( graph, start ) , coordToID ( graph, target ) );
	return succ;
}