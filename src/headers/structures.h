#ifndef STRUCTURES_H
#define STRUCTURES_H


#include "set.h"
#include <stdbool.h>
#include <ncurses.h>
#include <math.h>
#include <ncurses.h>
#include <menu.h>
#define WHITE 		0
#define GREY 		1
#define	BLACK 		2
#define INFINITE 	1000000
#define NIL             -1000000


extern bool DONE;
extern bool GAME_PAUSE;


typedef struct vectorPath {

    Set *coord;

} PATH;

typedef struct GraphT_Struct {
    int weight;
    int ID;
    int k;
} GraphT;

//map coordinate structure

typedef struct v_coord {
    int x;
    int y;
    int k; //value on the map ( 9 wall, 2 start... )
    int ID;
    bool path; //temporary field used to print the path

} VCOORD;

typedef struct gObj {
    //puntatore a matrice di adiacenza
    GraphT ** matrix;
    /*
    //puntatore a lista di adiacenza
    VERTEX 	* 		list;
     */
    //numero vertici
    int vNum; //number of vertices

    VCOORD ** maze; //pointer to implicit maze
    int width; //width of the maze
    int height; //height of the maze

    //function pointers
    struct gObj * (*build) (struct gObj *, int);
    Set * (*getAdjList) (struct gObj *graph, int u);

    void ( *insertEdge) (struct gObj *, int, int, int);
    void ( *insertVertex) (struct gObj *graph);

    int ( *getWeight) (struct gObj *, int, int);
    int ( *transpose) (struct gObj *graph);
    int * (*path) (struct gObj *, int, int);
} GRAPHOBJ;

//puntatori a funzione per operazioni varie su grafo: inserimento, 
//archi, vertici confronto, etc
typedef int ( *insDEF) (int, int);
typedef int ( *compDEF) (void *, void *);
typedef int ( *WDEF) (struct gObj *, int, int);
typedef int ( *TDEF) (GRAPHOBJ *graph);
typedef GRAPHOBJ * (*BDEF) (struct gObj *, int);
typedef void ( *IEDEF) (struct gObj *, int, int, int);
typedef void ( *VINS) (struct gObj *graph);
typedef Set * (*ADEF) (GRAPHOBJ *graph, int u);
typedef int * (*PATHD) (struct gObj *, int, int);

//oggetto cassa
typedef struct chest_st {
} CHEST;

typedef struct player_st {
    int damage;
    int health;
    int starting_health;
    int lives;
    int shots;
    int shot_range;
    float stamina;
    float speed;
    float clock;
    float c_counter;
    CHEST * chest;
	//coordinate di partenza
    VCOORD * base;
	//coordinate attuali
    VCOORD * coord;
	//coordinate di spostamento su percorso
    VCOORD * move_to;
	//percorso
    PATH * path;
	//stati
    bool moving;
    bool fired;
    //oggetto di stampa
	void * character;
	//lista delle armi
    Set *weapon_list;

	//funzione di stampa
    void ( *print) (WINDOW *win, struct player_st *player);

} PLAYER;

typedef void ( *playPRINT) (WINDOW *win, struct player_st *player);

typedef struct key_st {
    VCOORD * coord;
    bool taken;

} KEY;


typedef struct bonus_st {
    VCOORD * coord;
    int timer;
    int score;
} BONUS;

typedef struct weapon_st {
    int damage;
    bool taken;
    char * name;
    VCOORD * coord;
} WEAPON;

typedef struct trap_st {
    VCOORD * coord;
    int damage;
	int timer;
} TRAP;

//eplosione su mappa
typedef struct explosion_st {
    VCOORD * center;
    int counter;
    int limit;
    bool enabled;
} EXPLOSION;

typedef struct level_st {
    bool DONE;

    PLAYER * player;

    Set * key_list;
    Set * knight_list;
    Set * bonus_list;
    Set * trap_list;
    Set * weapon_list;
    
    VCOORD * start;
    VCOORD * target;
    GRAPHOBJ * graph;

    int keytogo;
    int weaponcount;
    int count; // numero di livelli
    char * map_filename;

    int tmp_counter;

    int difficulty;

} LEVEL;

typedef struct camera_st {
	//centro dello schermo
    int halfX;
    int halfY;
	//offset dalla mappa
    int hOffset;
    int vOffset;
	//lato
    VCOORD *edge;
	//focus della telecamera
    VCOORD *focus;
    void (*moveCamera) ( LEVEL *level, struct camera_st *); //gestione inquadratura
    
} CAMERA;
typedef void ( *CAM ) ( LEVEL *level, struct camera_st *cam );


typedef struct knight_st {
    int health;
    int slow_count; //contatore colpo di ritardo
    int damage;
    int difficulty;
    int range;
    float clock;
    float c_counter;
    float speed;
	//posizione della base o della chiave
    VCOORD  * base;
	//coordinata attuale
    VCOORD  * coord;
	//coordinata di spostamento
    VCOORD  * move_to;
	//percorso
    PATH    * path;
	//oggetto di riferimento
    KEY     * own_key;
	
	//stati del cavaliere
    bool acquired;
    bool follow;
    bool patrol;
    bool retreat;
    bool engage;
    bool blocked;
    bool moving;
	//carattere di stampa
    void * character;
	
	//callback a funzione di ricerca percorso minimo
    PATHD minPath;
    void ( *print ) ( WINDOW *win, struct knight_st *knight, CAMERA *cam );
    void ( *follow_behav ) ( WINDOW *win, LEVEL *level, struct knight_st *knight, PLAYER *player, PATHD alg );
    int ( *patrol_behav ) ( WINDOW *win, LEVEL *level, struct knight_st *knight, PLAYER *player, int size, PATHD alg );
    void ( *key_taken ) ( WINDOW *win, LEVEL* level, PLAYER *player, struct knight_st *knight );
    void ( *key_not_taken ) ( WINDOW *win, LEVEL* level, PLAYER *player, struct knight_st *knight );
    
} KNIGHT;

typedef void ( *knightPRINT ) (WINDOW *win, struct knight_st *knight, CAMERA *cam );
typedef void ( *knightBEHAVIOUR ) (WINDOW *win, LEVEL *level, struct knight_st *knight, PLAYER *player, PATHD alg );
typedef int ( *knightPATROL ) (WINDOW *win, LEVEL *level, struct knight_st *knight, PLAYER *player, int size, PATHD alg);
typedef void ( *KKBEV ) (WINDOW *win, LEVEL *level, struct knight_st *knight, PLAYER *player, PATHD alg );

#endif
