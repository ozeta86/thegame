#ifndef KNIGHT_H
#define KNIGHT_H
#include "./structures.h"

void knight_print ( WINDOW *win, KNIGHT *knight, CAMERA *cam );

int knight_patrol ( WINDOW *win, LEVEL *level, KNIGHT *knight, PLAYER *player, int size, PATHD alg );


void knight_follow ( WINDOW *win, LEVEL *level, KNIGHT *knight, PLAYER *player, PATHD alg );

void set_knight_state ( KNIGHT *knight, bool, bool patrol, bool engage );

void key_not_taken ( WINDOW *win, LEVEL* level, PLAYER *player, KNIGHT *knight);

void key_taken ( WINDOW *win, LEVEL* level, PLAYER *player, KNIGHT *knight ) ;

void knight_update ( WINDOW *win, LEVEL *level, KNIGHT *knight, PLAYER *player, PATHD alg );

void move_knight ( KNIGHT *knight );

#endif
