#ifndef GAME_H
#define GAME_H
#include "./structures.h"
#include "./knight.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))


//inizializza i colori della consolle
bool init_colors();

/**
 * pulisce lo schermo con un'animazione
 * @param win:					finestra da pulire
 * @param time:					tempo della transizione
 */
void animated_clear_screen(WINDOW *win, int time);

//inizializza la modalità ncurses
void inizialize_screen(void);
//stampa un messaggio sul bordo superiore di una finestra
void wprint_title_centered(WINDOW *win, const char *titleText);
/**
 * crea e visualizza un pannello di dialogo
 * @param title stringa titolo
 * @param message array di stringhe, terminato dalla stringa "NULL"
 */
void dialog_npanel(const char *title, char **message);
/**
 *  pannello di avviso
 * @param message		messaggio visualizzato
 */
void dialog_spanel(char *message);


/**
 * cancella il percorso attuale
 * @param path
 */
void empty_path(PATH *path);

/**
 * funzione di stampa del giocatore
 * @param win
 * @param player
 */
void print_player(WINDOW *win, PLAYER *player);
/**
 * aggiornamento del giocatore
 * @param level
 * @param player
 * @param cam
 */
void player_update(LEVEL *level, PLAYER *player, CAMERA *cam);



/**
 * restituisce un punto random nella mappa nel range di un centro
 * @param level
 * @param knight
 * @param center
 * @param size
 * @return 
 */
VCOORD get_random_coord ( LEVEL *level , VCOORD *center, int size );


/**
 * fase di duello: il giocatore attacca il cavaliere.
 * viene simulato un lancio di dadi 
 * @param win				finestra attiva di gioco
 * @param player
 * @param knight
 * @param stamina_cost		costo dell'attacco: decrementa la stamina
 * @param damageToKnight	danno inflitto al cavaliere
 * @param damageToPlayer	danno inflitto al giocatore
 * @param text_rows			array contentente le stringhe di output
 * @param xOffset			offset di stampa video
 */
void player_attacks(WINDOW *win, PLAYER *player, KNIGHT *knight, int weaponDamage, int yOffset, int xOffset);


void clrsrn(WINDOW *win, int yOffset, int xOffset, int j);
/**
 * fase di duello: il giocatore attacca il cavaliere.
 * viene simulato un lancio di dadi 
 * @param win				finestra di gioco attiva
 * @param player
 * @param knight
 * @param stamina_cost		costo della difesa: decrementa la stamina
 * @param text_rows			array contentente le stringhe di output
 * @param xOffset			offset di stampa video
 */

void player_defends(WINDOW *win, PLAYER *player, KNIGHT *knight, int stamina_cost, int yOffset, int xOffset);


/**
 * unisce due array di stringhe in un nuovo array, allocato dinamicante.
 * questo array viene usato nel menu dei battle_window
 * @param first			primo array
 * @param second		secondo array
 * @param numFirst		dimensione primo array
 * @param numSec		dimensione secondo array
 * @return 
 */
char **merge_strings_arrayss(char **first, char **second, int numFirst, int numSec);


/**
 * legge la lista delle armi e restituisce un array allocato dinamicamente di stringhe, 
 * composto dai nomi delle armi. E' utilizzato da battle_window per generare il menu
 * @param player
 * @return array coni nomi delle armi recuperate
 */
char **parse_weapons_list(Set *weapon_list);


/**
 * scorre la lista ed estrae il danno associato all'arma
 * @param set
 * @param index
 * @return 
 */
int get_damage_from_weapon(Set *set, int index);



/**
 * gestisce la battaglia tra giocatore e cavaliere nemico
 * @param win
 * @param index				indice estratto dal menu
 * @param n_choices			numero di scelte fisse ( schiva, para, scappa )
 * @param weaponNumber                  numero di armi
 * @param player
 * @param knight
 * @param weaponDamage                  livello di danno dell'arma scelta
 * @param yOffset			offset di stampa
 * @param xOffset			offset di stampa
 * @return 
 */
int battle(WINDOW *win, int index, int n_choices, int weaponNumber, PLAYER *player,
        KNIGHT *knight, int weaponDamage, int yOffset, int xOffset);


/**
 * outout del duello tra giocatore e cavaliere nemico
 */
void battle_output(WINDOW *win, PLAYER *player, KNIGHT *knight, int yOffset, int xOffset);


/**
 * libera la memoria usata dagli elementi della finestra
 */
void menu_free(MENU *menu, ITEM **items, int n_choices, char **weaponArray, int n_weapons, char **mergedArray);


/**
 * loop del duello
 * @param win
 * @param player
 * @param knight
 * @param menu
 * @param maxLenght			massima lunghezza stringa del menu
 * @param n_weapons			numero di armi
 * @param n_choices			scelte fisse del menu
 */
void battle_loop(WINDOW *win, PLAYER *player, KNIGHT *knight, MENU *menu, int maxLenght, int n_weapons, int n_choices);


/**
 * creazione finestra di duello
 * @param level
 * @param player
 * @param knight
 * @return 
 */
int battle_window(LEVEL *level, PLAYER *player, KNIGHT *knight);



/**
 * verifica che un oggetto sia presente nella visuale corrente
 * @param cam                   oggetto telecamera
 * @param target                oggetto target
 * @return			vero se l'oggetto è presente
 */
bool cam_bound_check(CAMERA *cam, VCOORD *target);


/**
 * stampa le entità presenti nel livello
 * @param win
 * @param level
 * @param player
 * @param cam
 */
void print_entities(WINDOW *win, LEVEL *level, PLAYER *player, CAMERA *cam);



/**
 * crea il pannello info
 * @param height			altezza del pannello
 * @return 
 */
WINDOW *new_info_panel(int height);


/**
 * aggiorna il pannello info
 * @param win
 * @param winResize			verifica se lo schermo è stato ridimensionato
 * @param panelHeight
 * @param level
 * @param i
 * @param player
 */
void info_update(WINDOW *win, bool winResize, int panelHeight, LEVEL *level, int i, PLAYER *player);


/**
 *cancellazione ricorsiva su lista concatenata
 *elimina i cavalieri uccisi nella partita 
 * @param knightList
 * @return 
 */

Set *remove_deads(Set *knightList);


/**
 * crea una nuova arma e la posiziona sulla mappa
 * @param level
 */
void new_random_weapon(LEVEL *level);


/**
 * aggiorna lo stato delle entità del livello
 * @param level
 * @param player
 */
void update_entities(LEVEL *level, PLAYER *player);


/**
 * verifica le condizioni di uscita dal gioco: player muore, termina vite o raggiunge
 * l'uscita
 * @param player
 * @param level
 * @param difficulty
 * @return 
 */
int exit_condition(PLAYER *player, LEVEL *level, int difficulty);


/**
 * loop principale del gioco
 * @param level: livello corrente
 * @param win: WINDOW in cui viene visualizzato il labirinto
 * @param info_panel: WINDOW in cui viene visualizzato il pannello delle info
 * @param cam: oggetto CAMERA
 * @param player: oggetto PLAYER
 * @param difficulty: difficoltà del livello
 * @param levelNumber: numero del livello
 * @return lo stato di uscita
 */
int game_loop(LEVEL *level, WINDOW *win, WINDOW *info_panel, int panelHeight, CAMERA *cam, PLAYER *player, int difficulty, int levelNumber);
/**
 * avvia il gioco: cicla le mappe presenti nel gioco, reimposta il giocatore, libera le risorse
 * @param level
 * @param levelNumber	livello di ingresso
 * @param difficulty	difficoltà inizale
 * @return 
 */
int new_game(LEVEL **level, int levelNumber, int difficulty);
/**
 * schermata iniziale di gioco. mostra il menu principale
 * @param level
 * @param x
 * @param y
 * @return 
 */
int game_menu(LEVEL **level, int x, int y);
/**
 * verifica che le coordinate date siano all'interno del grafo
 * @param graph
 * @param x
 * @param y
 * @return 
 */
bool boundCheck(GRAPHOBJ *graph, int x, int y);
/**
 * aggiorna lo stato della telecamera di gioco. la telecamera è centrata sul giocatore.
 * se il giocatore si trova all'interno dei margini stabiliti, la telecamera è ferma
 * @param cam
 * @param win
 * @param level
 */
void update_camera(CAMERA *cam, WINDOW *win, LEVEL *level);
/**
 * crea una nuova telecamera. segue il giocatore lungo la mappa
 * @return nuova telecamera
 */
CAMERA *new_camera();

/**
 * movimento della telecamera
 * @param cam
 * @param win
 * @param panelHeight
 * @param level
 */
void camera1(LEVEL *level, CAMERA *cam);


/**
 * aggiorna l'output a schermo
 * @param cam
 * @param win
 * @param panelHeight altezza del pannello INFO inferiore
 * @param level
 */
void print_maze(CAMERA *cam, WINDOW *win, int panelHeight, LEVEL *level);


/**
 * libera le risorse del giocatore
 * @param player	giocatore
 */
void free_player(PLAYER *player);

/**
 * legge da tastiera l'input di gioco dell'utente. seleziona il caso correlato
 * @param level
 * @param player
 * @param cam
 */
void player_driver(LEVEL *level, PLAYER *player, CAMERA *cam);

/**
 * inizializza le entità del gioco
 * @param level                 livello
 * @param player                giocatore
 * @param difficulty            difficoltà
 * @param file_name             nome del file della mappa
 * @param knight_following      comportamento del cavaliere
 * @return 
 */
void intialize_entities(LEVEL *level, int difficulty, knightBEHAVIOUR knight_follow);

/**
 *	inizializza il livello di gioco
 * @param level
 * @param player
 * @param difficulty	parametro di difficoltà
 * @param file_name	file di mappa
 * @return 
 */
LEVEL *initialize_level(LEVEL *level, PLAYER *player, int difficulty, char *file_name, knightBEHAVIOUR knight_following);
/**
 * inizializza l'oggetto giocatore
 * @param character		carattere di stampa
 * @param health		livello di vita
 * @param speed			velocità
 * @param chest			oggetto CASSA
 * @param x				coordinata x iniziale
 * @param y				coordinata y iniziale
 * @param print
 * @return oggetto PLAYER *
 */
PLAYER *init_player(char *character, int health, float speed, CHEST *chest, int x, int y, playPRINT print);
/**
 * inizializza l'oggetto cavaliere
 * @param character			carattere di stampa in output
 * @param health			livello di vita del cavaliere
 * @param speed				velocità di spostamento
 * @param clk				fattore di velocità
 * @param x					coordinata x iniziale
 * @param y					coordinata y iniziale
 * @param print				puntatore a funzione di stampa
 * @return	oggetto KNIGHT *
 */
KNIGHT *init_knight(char *character, int health, float speed, float clk, int x, int y, knightPRINT print, KEY *key, knightBEHAVIOUR);
/**
 * verifica la distanza tra 2 oggetti
 * @param a		oggetto
 * @param b		oggetto
 * @param dist	distanza
 * @return		vero se i due oggetti sono nel range richiesto
 */
bool get_collision_coord(VCOORD *a, VCOORD *b, int dist);
/**
 * pulisce il livello corrente cancellando tutte le entità create
 * @param level
 */
void clear_level(LEVEL *level);

#endif
