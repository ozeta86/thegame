#include <unistd.h>
#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <sys/time.h>
#include <ncurses.h>
#include <assert.h>
#include <menu.h>
#include <form.h>
#include "./headers/structures.h"
#include "./headers/game.h"
#include "./headers/functions.h"

#include "./headers/parser.h"
#include "./headers/pathfind.h"
#include "headers/lib.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

#define YELLOWONBLUE 	1
#define BLACKONWHITE 	2
#define WHITEONBLACK 	3
#define REDONBLACK   	4
#define WHITEONBLUE   	5
#define BLUEONYELLOW   	6
#define GREENONBLACK   	7
#define MAGENTAONBLACK	8
#define CYANONBLACK		9
#define BLACKONCYAN		10
#define BLACKONRED		11
#define ATTRIBS 		WA_BOLD

#define MARGINUP		4
#define SIDEMARGIN		4
#define NORTH(y) 		y - 1
#define SOUTH(y) 		y + 1
#define EAST(y) 		y + 1
#define WEST(y) 		y - 1

#define BYTES			128
#define MS_PER_UPDATE	20
#define KNIGHTHALT		200

//numero di facce del dado di gioco
#define DICEMAX			10
//costo stamina minimo
#define STAMINA_COST	10
//numero massimo di armi su livello
#define MAXWEAPONS		4

bool GAMEOVER 			= false;
bool GAME_PAUSE		 	= false;
bool FIRSTLAUNCH 		= true;


/******************************************************************************

inizializzazioni, testi delle finestre di dialogo e operazioni varie

******************************************************************************/

//array di strighe con stringa sentinella "NULL"

char *gameArray[] = {
	"   Sei morto!  ", 
	"", 
	"    Riprova!   ",
	"NULL"
};
char *combatArray[] = {
	"Sfida a duello", 
	"Tasti", 
	"",
	"NULL"
};
char *gameArray1[] = {
	"     The game.     ", 
	"", 
	"Hai vinto il gioco!",
	"",
	"Premi [INVIO] per tornare",
	" al menu",
	"NULL"
};

char *weaponsArray[] = { 
	"Spada semplice", 
	"Spada complessa", 
	"Pugnale semplice", 
	"Pugnale complesso", 
	"Alabarda semplice", 
	"Alabarda derivata", 
	"Mazza semplice", 
	"Mazza integrale", 
	"NULL"  
};
/*
char *weaponsArray[] = { 
	"Spada superlunghissima iper figa", 
	"NULL"  

};
*/

char *pauseArray[] = { 
	"Premi [Invio]", 
	"per continuare", 
	"NULL" 
};

//inizializza i colori della consolle
bool init_colors()
	{
	if(has_colors())
		{
		//AVVIA LA MODALITA COLORE
		start_color ();
		//INIZIALIZZA I TEMI
		init_pair ( YELLOWONBLUE 	, COLOR_YELLOW	, COLOR_BLUE 	);
		init_pair ( BLACKONWHITE 	, COLOR_BLACK 	, COLOR_WHITE 	);
		init_pair ( WHITEONBLACK 	, COLOR_WHITE 	, COLOR_BLACK 	);
		init_pair ( REDONBLACK   	, COLOR_RED		, COLOR_BLACK 	);
		init_pair ( WHITEONBLUE  	, COLOR_WHITE	, COLOR_BLUE 	);
		init_pair ( BLUEONYELLOW  	, COLOR_BLUE	, COLOR_YELLOW 	);
		init_pair ( GREENONBLACK  	, COLOR_BLACK	, COLOR_GREEN 	);
		init_pair ( MAGENTAONBLACK  , COLOR_MAGENTA	, COLOR_BLACK 	);
		init_pair ( CYANONBLACK  	, COLOR_CYAN	, COLOR_BLACK 	);
		init_pair ( BLACKONCYAN  	, COLOR_BLACK	, COLOR_CYAN 	);
		init_pair ( BLACKONRED  	, COLOR_BLACK	, COLOR_RED 	);

		return(true);
		}
	else
		return(false);
	}

/**
 * pulisce lo schermo con un'animazione
 * @param win:					finestra da pulire
 * @param time:					tempo della transizione
 */
void animated_clear_screen ( WINDOW *win, int time ) {
	int y, x, maxy, maxx;
	getmaxyx(win, maxy, maxx);
	for ( y=0; y < maxy/2 + 1; y++ ) {
		for ( x=0; x <= maxx; x++ ) {
			mvwaddch ( win, y, x, ' ' );
			mvwaddch ( win, maxy - y, maxx - x, ' ' );
			usleep ( time );
			wrefresh( win );
		}
	}
}

//inizializza la modalità ncurses
void inizialize_screen ( ) {
	initscr ( );
	//flush input utente
	flushinp ( );
	cbreak ( );
	noecho ( );
	//tasti funzione
	keypad ( stdscr, TRUE );
	init_colors ( );

	curs_set ( FALSE );
	//input non bloccante
	timeout ( 0 );

}

//stampa un messaggio sul bordo superiore di una finestra
void wprint_title_centered ( WINDOW *win, const char *titleText )	{
	int x, maxy, maxx;

	getmaxyx ( win,maxy,maxx );
	x =  ( maxx - 4 - strlen ( titleText ) )/2;
	mvwaddch ( win, 0, x, ACS_RTEE );
	waddch ( win, ' ' );
	waddstr ( win, titleText );
	waddch ( win, ' ' );
	waddch ( win, ACS_LTEE );
}

/**
 * crea e visualizza un pannello di dialogo
 * @param title stringa titolo
 * @param message array di stringhe, terminato dalla stringa "NULL"
 */
void dialog_npanel ( const char *title, char **message ) {
	//mette il gioco in pausa
	GAME_PAUSE = true;
	int i = 0;
	int maxLenght = 0;
	//calcola la lunghezza massima delle stringhe
	if ( message != NULL ) {
		while ( strcmp ( message[i], "NULL" ) != 0 ) {
			if ( strlen ( message[i] ) > maxLenght ) {
				maxLenght = strlen ( message[i] );
			}
			i++;
		}
	}
	//inizializzazioni
	int c;
	int maxy, maxx;
	getmaxyx ( stdscr, maxy, maxx );

	WINDOW 	*	dialogWin;
	PANEL 	*	dialogPanel;
	//dimensione pannello e creazione nuova finestra
	int lines = 7 + i + 1, cols = 10 + maxLenght, y = 2, x = 4;
	dialogWin = newwin ( lines, cols, ( maxy - lines ) / 2, ( maxx - cols ) / 2 );
	keypad ( dialogWin, TRUE ) ;
	box ( dialogWin, 0, 0) ;
	wprint_title_centered (dialogWin, title );
	dialogPanel = new_panel ( dialogWin );

	//stampa messaggio contenuto su array di stringhe
	if ( message != NULL ) {
		int j = 0;
		while ( j < i ) {
			mvwprintw ( dialogWin, y + j, x, "%s", message[j] );
			j++;
		}
	}
	//messaggio inferiore
	wattron ( dialogWin, COLOR_PAIR(1));
	mvwprintw ( dialogWin, lines - y - 1, x, "<Premi Invio>" );
	wattroff ( dialogWin, COLOR_PAIR(1));
	refresh ();
	update_panels ();
	doupdate ();
	//uscita finestra di dialogo
	timeout ( -1 );
	while ( ( c = getch () ) != '\n' ) {
		;
	}
	timeout ( 0 );
	del_panel( dialogPanel );
	delwin ( dialogWin );

}

/**
 *  pannello di avviso
 * @param message		messaggio visualizzato
 */
void dialog_spanel ( char *message ) {
	GAME_PAUSE = true;
	int i = 0;
	int maxLenght = 0;

	maxLenght = strlen ( message );
	
	int c;
	int maxy, maxx;
	getmaxyx ( stdscr, maxy, maxx );

	WINDOW 	*	dialogWin;
	PANEL 	*	dialogPanel;
	int lines = 7 + i + 1, cols = 10 + maxLenght, y = 2, x = 4;
	dialogWin = newwin ( lines, cols, ( maxy - lines ) / 2, ( maxx - cols ) / 2 );
	keypad ( dialogWin, TRUE ) ;
	box ( dialogWin, 0, 0) ;
	wprint_title_centered (dialogWin, "!" );
	dialogPanel = new_panel ( dialogWin );
	//stampa messaggio
	if ( message != NULL ) {
			mvwprintw ( dialogWin, y, x, "%s", message );
	}
	//messaggio inferiore
	wattron ( dialogWin, COLOR_PAIR(1));
	mvwprintw ( dialogWin, lines - y - 1, x, "<Premi Invio>" );
	wattroff ( dialogWin, COLOR_PAIR(1));
	refresh ();
	update_panels ();
	doupdate ();
	//uscita finestra di dialogo
	timeout ( -1 );
	while ( ( c = getch () ) != '\n' ) {
		;
	}
	timeout ( 0 );
	del_panel( dialogPanel );
	delwin ( dialogWin );

}

/**
 * muove un oggetto di una casella seguendo il percorso di tipo PATH
 * @param entity	coordinate dell'oggetto
 * @param path		percorso da seguire
 * @param speed		velocità ( parametro non implementato )
 */
void movePlayerTo ( PLAYER *player, PATH *path ) {
	VCOORD *entity = player->coord;
	assert ( entity );
	assert ( path );

	//simula la velocità del giocatore
	if ( player->c_counter > 0 ) {
		player->c_counter -= player->speed;
	} else {
		player->c_counter 	= player->clock;
		//estrae la posizione successiva		
		VCOORD *tmp = ( VCOORD * ) getFront ( path->coord );
		path->coord = dequeue ( path->coord );
		if ( tmp != NULL ) {
			player->coord->x 	= tmp->x;
			player->coord->y 	= tmp->y;
			free ( tmp );
		}
	}
}

/**
 * cancella il percorso attuale
 * @param path
 */
void empty_path ( PATH *path ) {
	if ( path != NULL ) {
		clearSet ( path->coord );
	}
	path = NULL;
}

/**
 * funzione di stampa del giocatore
 * @param win		oggetto finestra
 * @param player	oggetto giocatore
 */
void print_player ( WINDOW *win, PLAYER *player ) {
	mvwprintw ( win, (player->coord->y), (player->coord->x), player->character );
}

/**
 * aggiornamento del giocatore
 * @param level		oggetto livello
 * @param player	oggetto giocatore
 * @param cam		oggetto telecamera
 */
void player_update ( LEVEL *level, PLAYER *player, CAMERA *cam ) {

	//ricarica della stamina
	if ( player->health > 10 && player->stamina < 100 ) {
		player->stamina += 0.5;
	}

	//rilevamento movimento mouse
	if ( player->path->coord != NULL ) {
		//spostamento giocatores
		movePlayerTo ( player, player->path );
	}
}

/**
 * restituisce un punto random nella mappa nel range di un centro
 * @param level
 * @param knight
 * @param center
 * @param size
 * @return 
 */
VCOORD get_random_coord ( LEVEL *level , VCOORD *center, int size ) {
	VCOORD dest;
	dest.x = 0;
	dest.y = 0;
	//genero una nuova destinazione compresa in un'area di centro "center"
	dest.x = randomIntGenerator ( center->x - size, center->x + size );
	dest.y = randomIntGenerator ( center->y - size, center->y + size );
	//verifico che i punti appartengano alla mappa
	if ( dest.x <= 0 || dest.x >= level->graph->width ) {
		dest.x = center->x;
	}
	if ( dest.y <= 0 || dest.y >= level->graph->height ) {
		dest.y = center->y;
	}
	
	return dest;
}

/**
 * fase di duello: il giocatore attacca il cavaliere.
 * viene simulato un lancio di dadi 
 * @param win				finestra attiva di gioco
 * @param player			giocatore
 * @param knight			cavaliere
 * @param stamina_cost		costo dell'attacco: decrementa la stamina
 * @param damageToKnight	danno inflitto al cavaliere
 * @param damageToPlayer	danno inflitto al giocatore
 * @param text_rows			array contentente le stringhe di output
 * @param xOffset			offset di stampa video
 */
void player_attacks ( WINDOW *win, PLAYER *player, KNIGHT *knight,  int weaponDamage, int yOffset, int xOffset ) {

	int damageToKnight	= randomIntGenerator ( weaponDamage * 0.75, weaponDamage * 1.5 );
	int stamina_cost = damageToKnight * 0.75;
	int damageToPlayer = randomIntGenerator ( knight->damage / 3, knight->damage + knight->difficulty * 1.5 );
	clrsrn ( win, yOffset, xOffset, 4 );
	//verifica che ci sia sufficiente stamina per attaccare
	if ( player->stamina >= stamina_cost ) {

		//simulo lancio dadi: cavaliere viene colpito ma risponde
		int diceRoll = randomIntGenerator ( 1, DICEMAX );
		if ( diceRoll > DICEMAX / 2) {
			//genero i danni inferti al giocatore 
			//riduco la vita del cavaliere
			knight->health -= damageToKnight;
			//riduco la vita del giocatore
			player->health -= damageToPlayer;
			//notifica all'utente
			mvwprintw ( win, yOffset, xOffset, "Hai tolto %2d punti vita al cavaliere!", damageToKnight );
			mvwprintw ( win, yOffset + 1, xOffset, "Ma il cavaliere ha risposto all'attacco!" );
			mvwprintw ( win, yOffset + 2, xOffset, "Perdi %2d punti vita!", damageToPlayer );
		} else { //cavaliere viene colpito ma non risponde
			knight->health -= damageToKnight;
			mvwprintw ( win, yOffset, xOffset, "Hai tolto %2d punti vita al cavaliere!", damageToKnight );
			mvwprintw ( win, yOffset + 1, xOffset, "Il cavaliere non risponde all'attacco!" );
		}
		//decremento l'energia del giocatore a seguito il combattimento
		player->stamina -= diceRoll + stamina_cost ;
		
	//caso 2: stamina insufficiente
	} else {
		mvwprintw ( win, yOffset, xOffset, "hai esaurito la stamina e non puoi attaccare!" );
		mvwprintw ( win, yOffset + 1, xOffset, "puoi solo scappare!" );

	}

}

void clrsrn ( WINDOW *win, int yOffset, int xOffset, int j ) {
	int i;
	for ( i = 0; i < j; i++ ) {
		mvwprintw ( win, yOffset + i, xOffset, "\t\t\t\t\t\t\t" );
	}
}
/**
 * fase di duello: il giocatore attacca il cavaliere.
 * viene simulato un lancio di dadi 
 * @param win				finestra di gioco attiva
 * @param player			giocatore
 * @param knight			cavaliere
 * @param stamina_cost		costo della difesa: decrementa la stamina
 * @param text_rows			array contentente le stringhe di output
 * @param xOffset			offset di stampa video
 */

void player_defends ( WINDOW *win, PLAYER *player, KNIGHT *knight, int stamina_cost, int yOffset, int xOffset ) {
	clrsrn ( win, yOffset, xOffset, 4 );
	//c'è sufficiente stamina per difendere
	if ( player->stamina >= stamina_cost ) {
		//lancio dadi
		int diceRoll = randomIntGenerator ( 1, DICEMAX );
		
		//giocatore difende ma la difesa non è abbastanza efficace
		if ( diceRoll > DICEMAX / 2) {
			player->health -= ( randomIntGenerator ( knight->damage, knight->damage + knight->difficulty / 2 ) / 2 );
			mvwprintw ( win, yOffset, xOffset, "il giocatore difende!" );
			mvwprintw ( win, yOffset + 1, xOffset, "Ma la difesa non è efficace!" );
		
		//il giocatore blocca il colpo efficacemente
		} else {
			mvwprintw ( win, yOffset, xOffset, "colpo schivato!" );
		}
		//riduco l'energia del giocatore
		player->stamina -= diceRoll + stamina_cost ;
	} else {
		mvwprintw ( win, yOffset, xOffset, "hai esaurito la stamina e non puoi difendere!" );
		mvwprintw ( win, yOffset + 1, xOffset, "puoi solo scappare!" );
	
	}

}

/**
 * unisce due array di stringhe in un nuovo array, allocato dinamicante.
 * questo array viene usato nel menu dei battle_window
 * @param first			primo array
 * @param second		secondo array
 * @param numFirst		dimensione primo array
 * @param numSec		dimensione secondo array
 * @return				nuvo array di stringhe
 */
char ** merge_strings_arrays ( char **first, char **second, int numFirst, int numSec ) {
	char **new = NULL;
	new = ( char ** ) malloc ( ( numFirst + numSec + 1 ) * sizeof ( char * ) );
	memset ( new, 0, numFirst + numSec + 1 );
	int i = 0;
	int j = 0;
	for (; i < numFirst; i++ ) {
			new[i] = ( char * ) malloc ( BYTES * sizeof ( char ) );

			memset ( new[i], 0, BYTES );
			new[i] = strcpy ( new[i], first[i] );

	}
	for (; i < numFirst + numSec - 1; i++, j++ ) {
			new[i] = ( char * ) malloc ( BYTES * sizeof ( char ) );

			memset ( new[i], 0, BYTES );
			new[i] = strcpy ( new[i], second[j] );
		
	}
	new[i] = NULL;
	return new;
}
/**
 * legge la lista delle armi e restituisce un array allocato dinamicamente di stringhe, 
 * composto dai nomi delle armi. E' utilizzato da battle_window per generare il menu
 * @param player		giocatore
 * @return				array coni nomi delle armi estratte dalla lista
 */
char **parse_weapons_list ( Set *weapon_list ) {
	Set *tmp = weapon_list;
	int i;
	int u;
	char **res = ( char  ** ) malloc ( ( 1 + countItems ( weapon_list ) ) * sizeof ( char * ) );
	for ( i = 0; tmp != NULL; tmp = tmp->next, i++ ) {
		WEAPON *weapon = ( WEAPON * ) tmp->data;
		res[i] = ( char * ) malloc ( BYTES * sizeof ( char ) );
		if ( res[i] ) {
			memset ( res[i], 0, BYTES );
			sprintf ( res[i], "%2d: %s. Danno: %2d", i, weapon->name, weapon->damage );
		}
	}
	res[i] = NULL;
	return res;
}
/**
 * scorre la lista ed estrae il danno associato all'arma
 * @param set			lista delle armi
 * @param index			indice nella lista
 * @return				arma all'indice selezionato
 */
int get_damage_from_weapon ( Set *set, int index ) {
	Set *tmp = set;
	int result = -1;
	while (  tmp != NULL && index > 0 ) {
		tmp = tmp->next;
		index--;
	}
	if ( tmp != NULL ) {
		result = ( ( WEAPON * ) tmp->data)->damage;
	}
	return result;
}


/**
 * gestisce la battaglia tra giocatore e cavaliere nemico
 * @param win
 * @param index				indice estratto dal menu
 * @param n_choices			numero di scelte fisse ( schiva, para, scappa )
 * @param weaponNumber		numero di armi
 * @param player			giocatore
 * @param knight			cavaliere
 * @param weaponDamage		livello di danno dell'arma scelta
 * @param yOffset			offset di stampa
 * @param xOffset			offset di stampa
 * @return					stato della battaglia
 */
int battle ( WINDOW *win, int index, int n_choices, int weaponNumber, PLAYER *player, 
		KNIGHT *knight, int weaponDamage, int yOffset, int xOffset ) {
	n_choices--;
	bool END			= false;
	int default_choices = n_choices + weaponNumber;
	int maxRows			= 4;
	int i;
	int text_rows[maxRows];
	int stamina_cost;

	//se esiste un arma nella lista delle armi del giocatore ed è stata selezionata
	if ( index < weaponNumber ) {
		player_attacks (win, player, knight, weaponDamage, yOffset, xOffset);
	} else {
		if ( index == default_choices - 3 ) { //giocatore schiva
			stamina_cost = 4;
			player_defends (win, player, knight,  stamina_cost, yOffset, xOffset);
		} else if ( index == default_choices - 2 ) { //para con lo scudo
		
			stamina_cost = 7;
			player_defends (win, player, knight,  stamina_cost, yOffset, xOffset);
		} else if ( index == default_choices - 1 ) {	//scappa
			END = true;
		}
	}
	
	int res = 1;
	//player scappa
	if ( END == true ) {
		res = -1;
	//player muore
	} else if ( player->health <= 0 ) {
		dialog_spanel ( "Sei morto!" );
		res = 0;
	//player vince. aumenta di la "forza del giocatore".
	} else if ( knight->health <= 0 ) {
		dialog_spanel ( "Hai vinto il duello!" );
		player->damage++;
	}
	return res;
}

/**
 * outout del duello tra giocatore e cavaliere nemico
 * @param win			finestra
 * @param player		giocatore
 * @param knight		cavaliere
 * @param yOffset		offset di visualizzazione y
 * @param xOffset		offset di visualizzazione x
 */
void battle_output ( WINDOW *win, PLAYER *player, KNIGHT *knight, int yOffset, int xOffset ) {
	int i = 0;
	//pulisce l'area di output
	clrsrn ( win, yOffset, xOffset, 10 );
	box ( win, 0, 0 );
	wprint_title_centered ( win, "Duello" );
	mvwprintw ( win, win->_maxy - 2, 2, "[F1] per uscire" );
	mvwprintw ( win, yOffset, xOffset, "Health: %3d", player->health );
	mvwprintw ( win, yOffset + 2, xOffset, "Stamina: %3d", ( int ) player->stamina );
	mvwprintw ( win, yOffset + 4, xOffset, "Knight Health: %d", knight->health );
	for ( i = 0; i < ( 100 / 4 ); i++ ) {
		if ( i < ( player->health / 4 ) ) {
			wattron ( win, COLOR_PAIR ( BLUEONYELLOW | ATTRIBS ) );
			mvwprintw ( win, yOffset + 1, xOffset + i, " " );
			wattroff ( win, COLOR_PAIR ( BLUEONYELLOW | ATTRIBS ) );
		} else {
			wattron ( win, COLOR_PAIR ( BLACKONWHITE | ATTRIBS ) );
			mvwprintw ( win, yOffset + 1, xOffset + i, " " );
			wattroff ( win, COLOR_PAIR ( BLACKONWHITE | ATTRIBS ) );
		}
		if ( i < ( ( int ) player->stamina / 4 ) ) {
			wattron ( win, COLOR_PAIR ( BLACKONCYAN | ATTRIBS ) );
			mvwprintw ( win, yOffset + 3, xOffset + i, " " );
			wattroff ( win, COLOR_PAIR ( BLACKONCYAN | ATTRIBS ) );
		} else {
			wattron ( win, COLOR_PAIR ( BLACKONWHITE | ATTRIBS ) );
			mvwprintw ( win, yOffset + 3, xOffset + i, " " );
			wattroff ( win, COLOR_PAIR ( BLACKONWHITE | ATTRIBS ) );
		}
		if ( i < ( knight->health / 4 ) ) {
			wattron ( win, COLOR_PAIR ( BLACKONRED | ATTRIBS ) );
			mvwprintw ( win, yOffset + 5, xOffset + i, " " );
			wattroff ( win, COLOR_PAIR ( BLACKONRED | ATTRIBS ) );
		} else {
			wattron ( win, COLOR_PAIR ( BLACKONWHITE | ATTRIBS ) );
			mvwprintw ( win, yOffset + 5, xOffset + i, " " );
			wattroff ( win, COLOR_PAIR ( BLACKONWHITE | ATTRIBS ) );
		}
	}
}

/**
 * libera la memoria usata dagli elementi della finestra
 * @param menu				menu della finestra
 * @param items				oggetti della finestra
 * @param n_choices			dimensione array menu
 * @param weaponArray		array armi
 * @param n_weapons			dimensione array armi
 * @param mergedArray		array menu di gioco
 */
void menu_free ( MENU *menu, ITEM **items, int n_choices, char **weaponArray, int n_weapons, char **mergedArray ) {
	int i;
	unpost_menu ( menu );
	free_menu ( menu );
	for ( i = 0; i < n_choices; ++i )
		free_item ( items[i] );
	for ( i = 0; i < n_weapons; ++i )
		free ( weaponArray[i] );
	for ( i = 0; mergedArray[i] != NULL ; ++i ) {
		free ( mergedArray[i] );	
	}
	free ( weaponArray );
	free ( mergedArray );
}

/**
 * loop del duello
 * @param win				oggetto finestra NCURSES
 * @param player			giocatore
 * @param knight			cavaliere
 * @param menu				menu laterale
 * @param maxLenght			massima lunghezza stringa del menu
 * @param n_weapons			numero di armi
 * @param n_choices			scelte fisse del menu
 */
void battle_loop ( WINDOW *win, PLAYER *player, KNIGHT *knight, MENU *menu, int maxLenght, int n_weapons, int n_choices ) {
	int menuChoise = 0;
	int weaponDamage = -1;
	int res = 0;	
	int c;
	post_menu ( menu );
	battle_output ( win, player, knight, 10, maxLenght + SIDEMARGIN + 2 );
	wrefresh ( win );	
	while ( ( c = getch ( ) ) != KEY_F ( 1 ) && res != -1 && player->health > 0 && knight->health > 0 ) {
	
		switch ( c ) {
			case 's':
			case KEY_DOWN:
				menu_driver ( menu, REQ_DOWN_ITEM );
				break;
			case 'w':
			case KEY_UP:
				menu_driver ( menu, REQ_UP_ITEM );
				break;
			case ' ':
			case 10:
				menuChoise = item_index ( current_item ( menu ) );
				if ( menuChoise < n_weapons ) {
					weaponDamage = get_damage_from_weapon ( player->weapon_list, menuChoise );
				}
				res = battle ( win, menuChoise, n_choices, n_weapons, player, knight, weaponDamage, MARGINUP, maxLenght + SIDEMARGIN + 2 );				
			break;
		}
		//post_menu ( menu );
		battle_output ( win, player, knight, 10, maxLenght + SIDEMARGIN + 2 );
		refresh ( );
		wrefresh ( win );		
		wrefresh ( stdscr );			
	}	
}

/**
 * creazione finestra di duello
 * @param level
 * @param player
 * @param knight
 * @return 
 */
int battle_window ( LEVEL *level, PLAYER *player, KNIGHT *knight ) {
	char **weapons = parse_weapons_list ( player->weapon_list );
	ITEM **menu_items;
	MENU *my_menu;
	WINDOW *win;
	int n_choices, i;
	int maxLenght = 25;
	int maxHeight = 14;
	int max_y, max_x;
	//dimensioni finestra corrente
	getmaxyx ( stdscr, max_y, max_x );
	
	char *choices[] = {
		"-----------------------",
		"Schiva",
		"Difenditi con lo scudo",
		"Scappa",
		( char * ) NULL,
	};
	if ( FIRSTLAUNCH == true ) {
		FIRSTLAUNCH = false;
		//dialogNPanel ( "Guida sul duello", combatArray );
	}	
	//dimensione dell'array choices
	n_choices = ARRAY_SIZE ( choices );

	int n_weapons = 0;
	n_weapons = countItems ( player->weapon_list );
	char **mergedArray = merge_strings_arrays ( weapons, choices, n_weapons, n_choices );
	
	/* creo gli oggetti */
	menu_items = ( ITEM ** ) calloc ( n_choices + n_weapons, sizeof (ITEM * ) );
	for ( i = 0; mergedArray[i] != NULL ; ++i )
		menu_items[i] = new_item ( mergedArray[i], NULL );

	/* crea e associa menu a finestra*/
	win = newwin ( max_y, max_x, 0, 0 );
	keypad ( win, TRUE );
	my_menu = new_menu ( ( ITEM ** ) menu_items );
	set_menu_win ( my_menu, win );
	
	/*estrae la lunghezza massima delle stringhe contenute nell'array per
	* impostare le dimensioni degli oggetti nella finestra
	*/
	if ( mergedArray != NULL ) {
		for ( i = 0; i < n_weapons; i++ ) {
			if ( strlen ( weapons[i] ) > maxLenght ) {
				maxLenght = strlen ( weapons[i] );
			}
		}
	}	
	set_menu_sub ( my_menu, derwin ( win, maxHeight, maxLenght + SIDEMARGIN, MARGINUP, 2 ) );

	/* imposta il marker del menu */
	set_menu_mark ( my_menu, ">" );

	//loop di gioco
	battle_loop ( win, player, knight, my_menu, maxLenght, n_weapons, n_choices );

	/*libera la memoria dalle risorse impiegate nella nuova finestra*/
	menu_free ( my_menu, menu_items, n_choices, weapons, n_weapons, mergedArray );
	return 0;
}


/**
 * verifica che un oggetto sia presente nella visuale corrente
 * @param cam		oggetto telecamera
 * @param target	oggetto target
 * @return			vero se l'oggetto è presente
 */
bool cam_bound_check ( CAMERA *cam, VCOORD *target ) {
	bool result = false;
	if ( cam->focus->x + cam->halfX > abs ( target->x - cam->focus->x ) &&
		 cam->focus->y + cam->halfY > abs ( target->y - cam->focus->y ) )
	{
		result = true;
	}
	
	return result;
}
/**
 * stampa le entità presenti nel livello
 * @param win
 * @param level
 * @param player
 * @param cam
 */
void print_entities ( WINDOW *win, LEVEL *level, PLAYER *player, CAMERA *cam ) {

	Set *tmp = NULL;

	//STAMPA EXIT
	if ( level->keytogo == 0 ) {
		//se l'entità si trova all'interno della visuale
		if ( cam_bound_check ( cam, level->target ) == true ) {
			wattron ( win, COLOR_PAIR ( BLACKONWHITE | ATTRIBS ) );
			mvwprintw ( win, level->target->y - cam->vOffset, level->target->x - cam->hOffset, " " );
			wattroff ( win, COLOR_PAIR ( BLACKONWHITE | ATTRIBS ) );
		}
	}

	//STAMPA TRAPPOLE
	wattron ( win, COLOR_PAIR ( BLACKONCYAN | ATTRIBS ) );
	for ( tmp = level->trap_list; tmp != NULL; tmp = tmp->next ) {
		VCOORD *trap_coord = ((KEY*)tmp->data)->coord;
		if ( cam_bound_check ( cam, trap_coord ) == true ) {
			mvprintw ( trap_coord->y - cam->vOffset, trap_coord->x - cam->hOffset, " " );
		}
	}
	wattroff ( win, COLOR_PAIR ( BLACKONCYAN | ATTRIBS ) );

	//STAMPA BONUS
	//cambia aspetto del bonus una volta preso
	for ( tmp = level->bonus_list; tmp != NULL; tmp = tmp->next ) {
		BONUS *bonus = ( BONUS * ) tmp->data;
		if ( cam_bound_check ( cam, bonus->coord ) == true && bonus->timer > 0 ) {
			wattron ( win, COLOR_PAIR ( BLUEONYELLOW | ATTRIBS ) );
			mvprintw ( bonus->coord->y - cam->vOffset, bonus->coord->x - cam->hOffset, "!" );
			wattroff ( win, COLOR_PAIR ( BLUEONYELLOW | ATTRIBS ) );
		} else if ( cam_bound_check ( cam, bonus->coord ) == true && bonus->timer == 0 ) {
			wattron ( win, COLOR_PAIR ( REDONBLACK | WA_NORMAL) );
			mvprintw ( bonus->coord->y - cam->vOffset, bonus->coord->x - cam->hOffset, "-" );
			wattroff ( win, COLOR_PAIR ( REDONBLACK | WA_NORMAL) );
		}
	}

	//STAMPA CAVALIERE
	for ( tmp = level->knight_list; tmp != NULL; tmp = tmp->next ) {
		//stampa cavaliere
		KNIGHT *knight = (KNIGHT *) tmp->data;
		if ( cam_bound_check ( cam, knight->coord ) == true ) {		
			knight->print ( stdscr, knight,cam );
		}

	}

	//STAMPA ARMA NON RACCOLTA
	wattron ( win, COLOR_PAIR ( BLUEONYELLOW | ATTRIBS ) );
	for ( tmp = level->weapon_list; tmp != NULL; tmp = tmp->next ) {
		WEAPON *weapon = ( WEAPON * ) tmp->data;
		if ( cam_bound_check ( cam, weapon->coord ) == true && weapon->taken == false ) {		
			mvprintw ( weapon->coord->y - cam->vOffset, weapon->coord->x - cam->hOffset, "*" );
		}

	}	
	wattroff ( win, COLOR_PAIR ( BLUEONYELLOW | ATTRIBS ) );
	
	//STAMPA CHIAVI SE NON RAGGIUNTE
	//cambia aspetto della chiave una volta presa
	KEY *key;
	for ( tmp = level->key_list; tmp != NULL; tmp = tmp->next ) {
		key = ( KEY * ) tmp->data;

		if ( cam_bound_check ( cam, key->coord ) == true && key->taken == false ) {
			wattron ( win, COLOR_PAIR ( BLUEONYELLOW | ATTRIBS ) );
			mvprintw ( key->coord->y - cam->vOffset, key->coord->x - cam->hOffset, "K" );
			wattroff ( win, COLOR_PAIR ( BLUEONYELLOW | ATTRIBS ) );
		} else if ( cam_bound_check ( cam, key->coord ) == true && key->taken == true ) {
			wattron ( win, COLOR_PAIR ( REDONBLACK | ATTRIBS ) );
			mvprintw ( key->coord->y - cam->vOffset, key->coord->x - cam->hOffset, "k" );
			wattroff ( win, COLOR_PAIR ( REDONBLACK | ATTRIBS ) );
		}

	}
}

/**
 * crea il pannello info
 * @param height			altezza del pannello
 * @return 
 */
WINDOW *new_info_panel ( int height ) {
	WINDOW *win;
	int max_y, max_x;
	getmaxyx ( stdscr, max_y, max_x );
	win  = newwin ( height, max_x, max_y - height, 0 );
	box (  win , 0, 0 );
	return win;
}
/**
 * aggiorna il pannello info
 * @param win
 * @param winResize			verifica se lo schermo è stato ridimensionato
 * @param panelHeight
 * @param level
 * @param i
 * @param player
 */
void info_update ( WINDOW *win, bool winResize,int panelHeight, LEVEL *level, int i, PLAYER *player ) {
	if ( win ) {
		int y, x;
		Set *tmp;
		box ( win, 0, 0 );
		
		int maxy, maxx;
		getmaxyx ( stdscr, maxy, maxx );
		//se lo schermo è stato ridimensionato, ridimensiona il pannello info
		if ( winResize == true ) {
			mvwin ( win, maxy - panelHeight, 0 );
			
		}
		
		//STAMPA INFO
		mvwprintw ( win, 3, 48, "                            " );
		mvwprintw ( win, 2, 2, "Livello: %2d.\tSalute: %3d.\tChiavi mancanti: %2d.\tColpi: %2d.",
				i + 1, player->health, level->keytogo, player->shots );
		mvwprintw ( win, 3, 2, "Vite: %2d.\tStamina: %3d.", player->lives, ( int ) player->stamina );

		for ( tmp = level->knight_list; tmp != NULL; tmp = tmp->next ) {
			KNIGHT *knight = ( KNIGHT * ) tmp->data;
			if ( knight->slow_count > 0 ) {
				mvwprintw ( win, 3, 48, "CAVALIERE BLOCCATO -%3d-", knight->slow_count );
			}
		}
		for ( tmp = level->bonus_list; tmp != NULL; tmp = tmp->next ) {
			BONUS *bonus = ( BONUS * ) tmp->data;
			if ( get_collision_coord ( player->coord, bonus->coord, 0 ) && bonus->timer > 0 ) {
				mvwprintw ( win, 3, 48, "POWER UP!" );
			}
		}

	}
}

/**
 *cancellazione ricorsiva su lista concatenata
 *elimina i cavalieri uccisi nella partita 
 * @param knightList
 * @return 
 */

Set *remove_deads ( Set *knightList ) {

	if ( knightList != NULL ) {
		KNIGHT *knight = ( KNIGHT * ) knightList->data;
		if ( knight->health <= 0 ) {
			free ( knight->base );
			free ( knight->coord );
			free ( knight->move_to );
			free ( knight->path );
			free ( knight );
			knightList = knightList->next;
		} else {
			knightList->next = remove_deads ( knightList->next );
		}
	}
	return knightList;
}

/**
 * crea una nuova arma e la posiziona sulla mappa
 * @param level
 */
void new_random_weapon ( LEVEL *level ) {
	int x, y;
	int i = 0;
	//legge i nomi disponibili per le armi
	while ( strcmp ( weaponsArray[i], "NULL" ) != 0 ) {
		i++;
	}
	WEAPON *weapon = ( WEAPON *) malloc ( sizeof ( WEAPON ) );
	weapon->coord  = ( VCOORD * ) malloc ( sizeof ( VCOORD ) );
	weapon->damage = randomIntGenerator ( 2, level->difficulty + 10 );
	weapon->taken = false;
	weapon->name = weaponsArray[randomIntGenerator ( 0, i-1 )];
	level->weapon_list = push ( level->weapon_list, weapon );
	do {
		x = randomIntGenerator ( 1, level->graph->width );
		y = randomIntGenerator ( 1, level->graph->height );
	} while ( level->graph->maze[y - 1][x - 1].k == 9 );
	weapon->coord->x = x - 1;
	weapon->coord->y = y - 1;
}				
/**
 * aggiorna lo stato delle entità del livello
 * @param level
 * @param player
 */
void update_entities ( LEVEL *level, PLAYER *player ) {
	//AGGIORNA I CAVALIERI
	Set *tmp;
	level->knight_list = remove_deads ( level->knight_list );
	for ( tmp = level->knight_list; tmp != NULL; tmp = tmp->next ) {
		KNIGHT *knight = ( KNIGHT * ) tmp->data;
		knight_update ( stdscr, level, knight, player, a_star );
	}
	//player prende chiave
	VCOORD *key_coord;
	for ( tmp = level->key_list; tmp != NULL; tmp = tmp->next ) {
		key_coord = ( ( KEY* ) tmp->data )->coord;
		if ( get_collision_coord ( player->coord, key_coord, 0 ) ) {
			//cambia lo stato della chiave
			if ( ( ( KEY* ) tmp->data )->taken == false ) {
				( ( KEY* ) tmp->data )->taken = true;
				//decrementa il contatore delle chiavi da prendere
				level->keytogo--;
			}
		}
	}
	//player cade su trappola
	for ( tmp = level->trap_list; tmp != NULL; tmp = tmp->next ) {
		TRAP *trap = ( TRAP* ) tmp->data;
		if ( get_collision_coord ( player->coord, trap->coord, 0 ) ) {
			player->health = player->health - trap->damage;
		}
	}
	//player prende bonus
	for ( tmp = level->bonus_list; tmp != NULL; tmp = tmp->next ) {
		BONUS *bonus = ( BONUS* ) tmp->data;
		if ( get_collision_coord ( player->coord, bonus->coord, 0 ) && bonus->timer > 0 ) {
			player->health++;
			player->stamina += 0.5;
			bonus->timer--;
		}
	}
	//player prende arma
	for ( tmp = level->weapon_list; tmp != NULL; tmp = tmp->next ) {
		WEAPON *weapon = ( WEAPON * ) tmp->data;
		if ( get_collision_coord ( player->coord, weapon->coord, 0 ) && weapon->taken == false ) {
			weapon->taken = true;
			char message[256];
			sprintf ( message, "Hai preso un nuovo oggetto: %s", weapon->name );
			dialog_spanel ( message );
			player->weapon_list = push ( player->weapon_list, weapon );
		}
	}

}
/**
 * verifica le condizioni di uscita dal gioco: player muore, termina vite o raggiunge
 * l'uscita
 * @param player
 * @param level
 * @param difficulty
 * @return 
 */
int exit_condition ( PLAYER *player, LEVEL *level, int difficulty ) {
	//player muore: game over o reset su mappa
	int res = 0;
	if ( player->health <= 0 ) {
		player->lives--;
		if ( player->lives < 0 ) {
			GAMEOVER = true;
			dialog_npanel ( "Game over!", gameArray1 );
			res = -1;
		} else {
			player->coord->x = player->base->x;
			player->coord->y = player->base->y;
			player->starting_health -= difficulty;
			player->health = player->starting_health;
			dialog_npanel ( "Oops!", gameArray );
		}
	}
	//player raggiunge exit
	if ( get_collision_coord ( player->coord, level->target, 0 ) ) {
		if ( level->keytogo == 0 ) {
			level->DONE = true;
			res = 1;
		}
	}
	return res;
}

double get_current_time ( ) {
	struct timeval tval;

	gettimeofday ( &tval, NULL );
	double msec = (tval.tv_sec) * 1000 + (tval.tv_usec) / 1000 ;
	return msec;
}
/**
 * loop principale del gioco
 * @param level: livello corrente
 * @param win: WINDOW in cui viene visualizzato il labirinto
 * @param info_panel: WINDOW in cui viene visualizzato il pannello delle info
 * @param cam: oggetto CAMERA
 * @param player: oggetto PLAYER
 * @param difficulty: difficoltà del livello
 * @param levelNumber: numero del livello
 * @return lo stato di uscita
 */
int game_loop ( LEVEL *level, WINDOW *win, WINDOW *info_panel, int panelHeight, CAMERA *cam, PLAYER *player, int difficulty, int levelNumber ) {
	
	int res = 0;
	double current;
	double previous = get_current_time ( );
	double elapsed;
	double lag = 0.0;
	bool winResize;

	int newy = 0,  newx = 0;
	int curry = 0, currx = 0;
	while ( level->DONE == false && GAMEOVER == false ) {
		//calcolo il tempo trascorso dall'ultimo aggiornamento dello stato del gioco
		current = get_current_time ( );
		elapsed = current - previous;
		previous = current;
		//lag è la quantità di tempo che devo simulare per
		//sincronizzare il tempo del gioco e il tempo "reale"
		lag += elapsed;

		//input player
		player_driver ( level, player, cam );

		//aggiorno il gioco con uno step fissato di tempo fino ad esaurire il lag
		while ( lag >= MS_PER_UPDATE ) {
			//aggiorna lo stato delle entità
			player_update ( level, player, cam );
			update_entities ( level, player );
			//controlla le condizioni di reset del giocatore e della fine del gioco
			res = exit_condition ( player, level, difficulty );
			lag -= MS_PER_UPDATE;
		}
	
		//rilevo un eventuale resize della finestra del terminale
		getmaxyx ( stdscr, newy, newx );
		if ( newy != curry || newx != currx ) {
			winResize = true;
		} else {
			winResize = false;
		}
		curry = newx;
		currx = newy;
		//aggiorna pannello info
		info_update ( info_panel, winResize, panelHeight, level, levelNumber, player );
		
		//eseguo il refresh dello schermo
		print_maze ( cam, win, panelHeight, level );
		wnoutrefresh ( win );
		wrefresh ( info_panel );
		doupdate();
		//riprende la simulazione di gioco a seguito di una pausa comandata
		if ( GAME_PAUSE == true ) {
			GAME_PAUSE = false;
			previous = get_current_time ( );
		}
	}


	
	return res;
}
/**
 * libera le risorse del giocatore
 * @param player	giocatore
 */
void free_player ( PLAYER *player ) {
	free ( player->base );
	free ( player->coord );
	free ( player->move_to );
	clearSet ( player->weapon_list );
	empty_path ( player->path );
	free ( player );
}
/**
 * avvia il gioco: cicla le mappe presenti nel gioco, reimposta il giocatore, libera le risorse
 * @param level
 * @param levelNumber	livello di ingresso
 * @param difficulty	difficoltà inizale
 * @return 
 */
int new_game ( LEVEL **level, int levelNumber, int difficulty ) {
	GAMEOVER = false;
	mousemask(ALL_MOUSE_EVENTS, NULL);
	srand(time(NULL));
	int max_y = 0, max_x = 0;
	int res = 0;
	getmaxyx( stdscr, max_y, max_x);

	int			x			= max_x / 2;
	int			y 			= max_y / 2;
	int			panelHeight = 5;
	Set		*	tmp			= NULL;
	PLAYER  * 	player		= level[0]->player;
	WINDOW 	*	info_panel 	= new_info_panel ( panelHeight );
	WINDOW 	*	main		= newwin ( max_y - panelHeight - 1, max_x, 0, 0 );
	CAMERA	*	cam			= new_camera ( camera1 );
	//my_panels[0]			= new_panel(main);
	//my_panels[1]			= new_panel(info_panel);
	for ( ; levelNumber < level[0]->count && GAMEOVER == false ; levelNumber++ ) {
		
		assert ( level[levelNumber] );
		//pulisce lo schermo
		clear();
		//reimposta il giocatore sulla nuova mappa
		player->coord->x 	= level[levelNumber]->start->x;
		player->coord->y 	= level[levelNumber]->start->y;
		player->base->x 	= level[levelNumber]->start->x;
		player->base->y 	= level[levelNumber]->start->y;


		//ciclo di gioco principale
		res = game_loop ( level[levelNumber], stdscr, info_panel, panelHeight, cam, player, difficulty, levelNumber );
		animated_clear_screen ( stdscr, 150 );
		clear_level ( level[levelNumber] );
		//res++;
	}
	free ( cam );
	free_player ( player );
	delwin ( info_panel );
	return res;
}
/**
 * schermata iniziale di gioco. mostra il menu principale
 * @param level
 * @param x
 * @param y
 * @return 
 */
int game_menu ( LEVEL **level, int x, int y ) {
	
	char *choices[] = {
		/*00*/"New Game -> Easy",
		/*01*/"New Game -> Medium",
		/*02*/"New Game -> Hard",
		// /*03*/"load Map",
		/*04*/"Quit",
	};

	int c;
	int n_choices, i;
	int resChoise = 0;
	int maxy, maxx;
	getmaxyx ( stdscr, maxy, maxx );
	ITEM ** menuItems;
	MENU * mainMenu;
	WINDOW * win;
	/**
	 * creazione della nuova finestra ed inizializzazioni
     */
	n_choices = ARRAY_SIZE ( choices );
	menuItems = ( ITEM ** ) calloc ( n_choices + 1, sizeof ( ITEM * ) );
	for ( i = 0; i < n_choices; ++i ) {
		menuItems[i] = new_item ( choices[i], NULL );
	}
	menuItems[n_choices] = ( ITEM * ) NULL;
	mainMenu = new_menu ( menuItems );
	win = newwin ( maxy, maxx, 0, 0 );
	keypad ( win, TRUE );
	set_menu_win ( mainMenu, win );
	int lines = 15, cols = 20;
	set_menu_sub ( mainMenu, derwin ( win, lines, cols, ( maxy - lines ) / 2, ( maxx - cols ) / 2 ) );
	set_menu_mark ( mainMenu, ">" );
	box ( win, 0, 0 );
	wprint_title_centered ( win, "Thegame" );
	refresh ( );
	//touchwin( win );
	post_menu ( mainMenu );
	wrefresh ( win );
	int res = 0;
	//ciclo di input utente
	while ( res != -1 ) {
		c = getch ( );
		switch ( c ) {
			//cursore giù
			case 's':
			case KEY_DOWN:
				menu_driver ( mainMenu, REQ_DOWN_ITEM );
				break;
			//cursore su
			case 'w':
			case KEY_UP:
				menu_driver ( mainMenu, REQ_UP_ITEM );
				break;
			// Enter //
			case 10:
				resChoise = item_index ( current_item ( mainMenu ) );
				mvprintw ( LINES - 3, 20, "Last selection: %d", resChoise );
				res = -1;
				break;
			default:
				break;
		}
		box ( win, 0, 0 );
		wprint_title_centered ( win, "Thegame" );
		post_menu ( mainMenu );
		refresh ( );
		//touchwin( win );
		wrefresh ( stdscr );
		wrefresh ( win );

	}

	//end routine
	//del_panel( p->panel );
	delwin ( win );
	free_item ( menuItems[0] );
	free_item ( menuItems[1] );
	unpost_menu ( mainMenu );
	free_menu ( mainMenu );
	//timeout ( 0 );
	return resChoise;
}
/**
 * verifica che le coordinate date siano all'interno del grafo
 * @param graph
 * @param x
 * @param y
 * @return 
 */
bool boundCheck ( GRAPHOBJ *graph, int x, int y ) {
	bool result = true;
	if ( x < 0 || y < 0 ) {
		result = false;
	}
	
	if ( x >= graph->width || y >= graph->height ) {
		result = false;
	}
		
	return result;
}

void camera1 ( LEVEL *level, CAMERA *cam ) {

	VCOORD *pCoord = level->player->coord;
	GRAPHOBJ *graph = level->graph;
	//margine di spostamento
	int margin = 20;
	//spostamento orizzontale
	if ( pCoord->x < margin ) {
		//telecamera fissa su lato sx mappa
		cam->hOffset = margin - cam->halfX;
	} else if ( graph->width - pCoord->x < margin ) {
		cam->hOffset = graph->width - margin - cam->halfX;	
	} else {
		//telecamera segue giocatore
		cam->hOffset = cam->focus->x - cam->halfX;
	}
	
	  
		//cam->hOffset = cam->center->x - cam->halfX;
	//spostamento verticale
		int yMargin = margin / 2 -4;

	if ( pCoord->y < yMargin ) {
		cam->vOffset = yMargin - cam->halfY;
	} else if ( graph->height - pCoord->y < yMargin ) {
		cam->vOffset = graph->height - yMargin - cam->halfY;
	} else {
		cam->vOffset = cam->focus->y - cam->halfY;
	}	
	
	
}

/**
 * aggiorna lo stato della telecamera di gioco. la telecamera è centrata sul giocatore.
 * se il giocatore si trova all'interno dei margini stabiliti, la telecamera è ferma
 * @param cam
 * @param win
 * @param graph
 * @param level
 */
void update_camera ( CAMERA *cam, WINDOW *win, LEVEL *level ) {
	cam->focus = level->player->coord;
	
	int i, j, max_y, max_x;
	getmaxyx ( win, max_y, max_x );
	
	//coordinata centro schermo
	cam->halfX = max_x / 2;
	cam->halfY = max_y / 2;

	cam->moveCamera ( level, cam );
}

/**
 * crea una nuova telecamera. segue il giocatore lungo la mappa
 * @return nuova telecamera
 */
CAMERA *new_camera ( CAM moveCamera ) {
	
	CAMERA *cam = ( CAMERA * ) malloc ( sizeof ( CAMERA ) );
	cam->moveCamera = moveCamera;
	return cam;
}


/**
 * aggiorna l'output a schermo
 * @param cam
 * @param win
 * @param panelHeight altezza del pannello INFO inferiore
 * @param graph
 * @param level
 */
void print_maze ( CAMERA *cam, WINDOW *win, int panelHeight, LEVEL *level ) {
	//aggiorna la telecamera
	update_camera ( cam, win, level);
	int i, j, max_y, max_x;
	getmaxyx ( stdscr, max_y, max_x );
	GRAPHOBJ *graph = level->graph;
	//stampa la geometria della mappa
	for ( i = 0; i < max_y - panelHeight; i++ ) { //righe
		for ( j = 0; j < max_x; j++ ) { //colonne
			//verifica che l'elemento sia all'interno della visuale
			if ( boundCheck ( graph, cam->hOffset + j, cam->vOffset + i ) ) {
				//stampa corridoio
				if ( graph->maze[cam->vOffset + i][cam->hOffset + j].k <= 3 ) {
					mvwprintw ( win, i, j, " " );
				//stampa muro
				} else if ( graph->maze[cam->vOffset + i][cam->hOffset + j].k == 9 ) {
					attron ( COLOR_PAIR ( YELLOWONBLUE | ATTRIBS ) );
					mvwprintw ( win, i, j, " " );
					attroff ( COLOR_PAIR ( YELLOWONBLUE | ATTRIBS ) );
				}
			//stampa bordo esterno mappa	
			} else {
				attron ( COLOR_PAIR ( BLACKONWHITE | ATTRIBS ) );
				mvwprintw ( win, i, j, " " );
				attroff ( COLOR_PAIR ( BLACKONWHITE | ATTRIBS ) );
			}
		}
	}
	
	//stampa gli oggetti nel gioco
	print_entities ( win, level, level->player, cam );	
	//STAMPA PLAYER
	mvwprintw ( win, level->player->coord->y - cam->vOffset, level->player->coord->x - cam->hOffset, "o" );	

}
/**
 * legge da tastiera l'input di gioco dell'utente. seleziona il caso correlato
 * @param level
 * @param player
 * @param cam
 */
void player_driver ( LEVEL *level, PLAYER *player, CAMERA *cam ) {

	//inizializzazioni
	MEVENT		event;
	VCOORD ** 	maze 		= level->graph->maze;
	int			y 			= player->coord->y;
	int			x 			= player->coord->x;
	int			c;
	player->move_to->x		= -1;
	player->move_to->y		= -1;
	player->fired			= false;

	if ( ( c = getch()) != ERR ) {
		switch(c) {
			//muove su
			case KEY_UP:
			case 'w':
				player->moving = false;
				if ( y == 0 && maze[level->graph->height -1][x].k != 9 ) {
					player->coord->y = level->graph->height -1;
				} else if ( y > 0 && maze[NORTH(y)][x].k != 9 ) {
					--player->coord->y;
				}
				break;
			//muove giu
			case KEY_DOWN:
			case 's':
				player->moving = false;
				if ( y == level->graph->height -1 && maze[0][x].k != 9 ) {
					player->coord->y = 0;
				} else if ( y < level->graph->height -1 && maze[SOUTH(y)][x].k != 9 ) {
					++player->coord->y;
				}
				break;
			//muove a sinistra
			case KEY_LEFT:
			case 'a':
				player->moving = false;
				if ( x == 0 && maze[y][level->graph->width -1].k != 9) {
					player->coord->x = level->graph->width -1;
				} else if ( x > 0 && maze[y][WEST(x)].k != 9 ) {
					--player->coord->x;
				}	
				break;
			//muove a destra
			case KEY_RIGHT:
			case 'd':
				player->moving = false;
				if ( x == level->graph->width -1 && maze[y][0].k != 9) {
					player->coord->x = 0;
				} else if ( x < level->graph->width -1 && maze[y][EAST(x)].k != 9) {
					++player->coord->x;
				}
				break;		
			//spara il colpo di ritardo e verifica se ci sono bersagli utili
			case ' ':
				if ( player->shots > 0 ) {
					player->fired = true;
					player->shots--;
					Set *tmp;
					for ( tmp = level->knight_list; tmp != NULL; tmp = tmp->next ) {
						KNIGHT *knight = ( KNIGHT * ) tmp->data;
						if ( get_collision_coord ( player->coord, knight->coord, 16 ) ) {
							knight->blocked = true;
						}
					}
				}
				
				break;
				// corre a destra seguendo un percorso minimo
			case 'r':
				clearSet ( player->path->coord );
				player->move_to->x = player->coord->x + 4;
				player->move_to->y = player->coord->y;
				player->path->coord = pathfind ( level->graph, player->coord, player->move_to, a_star );
				player->moving = true;				
				break;
				//PATHFIND
			case 'f': 
				break;	
			//input del mouse
			case KEY_MOUSE:
				if ( getmouse(&event) == OK ) {	
					//tasto sinistro
					if ( event.bstate & BUTTON1_CLICKED ) {
						//estraggo le coordinate del mouse, verifico che siano valide
						if ( event.x + cam->hOffset>= 0 && event.x + cam->hOffset< level->graph->width &&
							 event.y + cam->vOffset>= 0 && event.y + cam->vOffset< level->graph->height &&
							maze[event.y + cam->vOffset][event.x + cam->hOffset].k != 9 ) {
							//svuoto il percorso attuale del giocatore e creo un nuovo percorso
							clearSet ( player->path->coord );
							player->move_to->x = event.x + cam->hOffset;
							player->move_to->y = event.y + cam->vOffset;
							player->path->coord = pathfind ( level->graph, player->coord, player->move_to, a_star );
							player->moving = true;
						}
					//tasto destro
					} else if ( event.bstate & BUTTON2_CLICKED ) {
						
					}
				}
			break;
			//pausa
			case 'p':
				GAME_PAUSE = true;
				dialog_npanel ( "Pausa", pauseArray );

			break;
			//skippa livello ( tasto F1 )
			case KEY_F(1):
				level->DONE = true;
			break;
			//termina gioco ( tasto F10 )
			case KEY_F(10):
				GAMEOVER = true;
				break;
			default:
				break;
		}
	} 

}

/**
 * legge i vari oggetti dalla mappa e li alloca ed inizializza
 * @param level
 * @param difficulty
 */
void intialize_entities ( LEVEL *level, int difficulty, knightBEHAVIOUR knight_follow ) {
	int i, j, randx, randy;

	//lettura del grafo associato alla mappa
	for ( i = 0; i < level->graph->height; i++ ) {
		for ( j = 0; j < level->graph->width; j++ ) {
			//ricerca start
			if ( level->graph->maze[i][j].k == 2 ) {
				level->start->x		= j;
				level->start->y		= i;

			//ricerca target
			} else if ( level->graph->maze[i][j].k == 3 ) {
				level->target->x	= j;
				level->target->y	= i;

			} else if ( level->graph->maze[i][j].k == 4 ) {
				KEY *key			= ( KEY * ) malloc ( sizeof ( KEY ) );
				key->coord			= ( VCOORD * ) malloc ( sizeof ( VCOORD ) );
				
				assert ( key );
				assert ( key->coord );
				key->coord->x		= j;
				key->coord->y		= i;
				key->taken			= false;
				//LIST*
				level->key_list		= push ( level->key_list, key );
				level->keytogo++;
				//posiziona cavaliere (coordinate iniziali, base)
				char *knightstr		= ( char * ) malloc ( 20 * sizeof ( char ) );
				assert ( knightstr );
				sprintf ( knightstr, "%c", 'A' + level->keytogo - 1 );
				int speed			= randomIntGenerator ( 1, difficulty * 1.5 );
				KNIGHT *knight		= init_knight ( knightstr, 100, speed, 2, -1, -1, knight_print, key, knight_follow );

				VCOORD dest;
				do {
					dest = get_random_coord ( level, key->coord, 3 );
				} while ( level->graph->maze[dest.y][dest.x].k == 9 );

				knight->coord->x 	= dest.x;
				knight->coord->y 	= dest.y;
				knight->base->x	 	= j;
				knight->base->y 	= i;
				knight->difficulty 	= difficulty;
				level->knight_list 	= push ( level->knight_list, knight );
			} else if ( level->graph->maze[i][j].k == 5 ) {
				TRAP *trap 			= ( TRAP * ) malloc ( sizeof ( TRAP ) );
				trap->coord 		= ( VCOORD * ) malloc ( sizeof ( VCOORD ) );
				assert ( trap );
				assert ( trap->coord );
				trap->coord->x 		= j;
				trap->coord->y 		= i;
				trap->damage 		= 4 + difficulty;
				level->trap_list 	= push ( level->trap_list, trap );
			} else if ( level->graph->maze[i][j].k == 6 ) {
				BONUS *bonus 		= ( BONUS * ) malloc ( sizeof ( BONUS ) );
				bonus->coord		= ( VCOORD * ) malloc ( sizeof ( VCOORD ) );
				assert ( bonus );
				assert ( bonus->coord );
				bonus->coord->x 	= j;
				bonus->coord->y 	= i;
				bonus->timer 		= 20 - difficulty;
				level->bonus_list 	= push ( level->bonus_list, bonus );
			}
		}

	}
	
	//inserisce N oggetti in una posizione random
	while ( level->weaponcount < MAXWEAPONS ) {
			new_random_weapon ( level );
			level->weaponcount++;	
	}
}
/**
 *	inizializza il livello di gioco
 * @param level
 * @param player
 * @param difficulty	parametro di difficoltà
 * @param file_name	file di mappa
 * @return 
 */
LEVEL *initialize_level ( LEVEL *level, PLAYER *player, int difficulty, char *file_name, knightBEHAVIOUR knight_following ) {
	//inizializzazione livello
	level 						= ( LEVEL *) malloc ( sizeof ( LEVEL ) );
	level->start 				= ( VCOORD * ) malloc ( sizeof ( VCOORD ) );
	level->target 				= ( VCOORD * ) malloc ( sizeof ( VCOORD ) );	
	assert ( level );
	assert ( level->start );
	assert ( level->target );
	
	level->key_list 			= NULL;
	level->knight_list 			= NULL;
	level->trap_list			= NULL;
	level->bonus_list			= NULL;
	level->weapon_list			= NULL;
	level->DONE 				= false;
	
	//inizializzazione grafo. sono impiegati i callback alle varie funzioni
	level->graph  				= initializeGraph ( buildGraph, insertEdgeIntograph, 
									getMatrixWeight, matrixInsertInterface, 
									NULL, getAdjList, NULL );
	level->player 				= player;
	level->map_filename 		= file_name;
	level->keytogo 				= 0;
	level->weaponcount 			= 0;
	level->difficulty 			= difficulty;
	//lettura mappa da file
	char 	*	mazeStr 		= NULL;
	mazeStr 					= mazeToString ( level->graph, level->map_filename );	
	if ( !mazeStr ) {
		endwin();
		printf ("\n\nImpossibile trovare il file mappa, controllare il percorso.\n\nEsecuzione annullata.\n");
		exit ( -1 );
	};
		

	//inizializzazione Start e Target

	level->start->x 			= level->start->y = -1;
	level->target->x 			= level->target->y = -1;
	//implicit map callbacks
	level->graph->getWeight 	= getMazeWeight;
	level->graph->getAdjList 	= getMazeAdjList;
	//creazione mappa		
	level->graph->maze 			= buildMap ( level->graph, mazeStr );

	assert ( level->graph->maze != NULL );	
	//inizializzazione mappa, coordinate start e target, nemici, chiavi
	//bonus e trappole
	intialize_entities ( level, difficulty, knight_following );
	player->coord->x 			= level->start->x;
	player->coord->y 			= level->start->y;
	player->lives 				= 5;
	player->starting_health		= 100;
	player->damage				-= difficulty / 2;
	//se la difficoltà rende negativo il danno, viene normalizzato
	if ( player->damage < 0 ) {
		player->damage = 1;
	}
	level->tmp_counter 			= 10;

	return level;
}

/**
 * inizializza l'oggetto giocatore
 * @param character		carattere di stampa
 * @param health		livello di vita
 * @param speed			velocità
 * @param chest			oggetto CASSA
 * @param x				coordinata x iniziale
 * @param y				coordinata y iniziale
 * @param print
 * @return oggetto PLAYER *
 */
PLAYER *init_player ( char *character, int health, float speed, CHEST *chest, int x, int y, playPRINT print ) {
	
	PLAYER *player		= ( PLAYER 	* ) malloc ( sizeof ( PLAYER ) );
	player->path 		= ( PATH 	* ) malloc ( sizeof ( PATH   ) );
	player->base		= ( VCOORD 	* ) malloc ( sizeof ( VCOORD ) );
	player->coord		= ( VCOORD 	* ) malloc ( sizeof ( VCOORD ) );
	player->move_to		= ( VCOORD 	* ) malloc ( sizeof ( VCOORD ) );
	
	assert ( player );
	assert ( player->path );
	assert ( player->base );
	assert ( player->coord );
	assert ( player->move_to );
	
	player->path->coord	= NULL;
	player->weapon_list	= NULL;

	player->chest		= chest;
	player->speed 		= speed;
	player->clock		= 2;
	player->health 		= health;
	player->character 	= character;
	player->coord->x 	= x;
	player->coord->y 	= y;
	player->move_to->x 	= -1;
	player->move_to->y 	= -1;
	player->shots		= 10;
	player->shot_range 	= 10;
	player->print 		= print;
	player->damage		= 10;
	player->stamina		= 100.0;
	player->fired		= false;
	return player;
}

/**
 * inizializza l'oggetto cavaliere
 * @param character			carattere di stampa in output
 * @param health			livello di vita del cavaliere
 * @param speed				velocità di spostamento
 * @param clk				fattore di velocità
 * @param x					coordinata x iniziale
 * @param y					coordinata y iniziale
 * @param print				puntatore a funzione di stampa
 * @return	oggetto KNIGHT *
 */
KNIGHT *init_knight ( char *character, int health, float speed, float clk, int x, int y, knightPRINT print, KEY *key, knightBEHAVIOUR follow ) {

	KNIGHT *knight			= ( KNIGHT 	* ) malloc ( sizeof ( KNIGHT ) );
	knight->path			= ( PATH 	* ) malloc ( sizeof ( PATH   ) );
	knight->base			= ( VCOORD 	* ) malloc ( sizeof ( VCOORD ) );
	knight->coord			= ( VCOORD 	* ) malloc ( sizeof ( VCOORD ) );
	knight->move_to			= ( VCOORD 	* ) malloc ( sizeof ( VCOORD ) );
	
	assert ( knight );
	assert ( knight->path );
	assert ( knight->base );
	assert ( knight->coord );
	assert ( knight->move_to );
	
	knight->own_key			= key;
	knight->path->coord		= NULL;
	knight->speed			= speed;
	knight->health			= health;
	knight->character		= character;
	knight->damage			= 5;
	knight->difficulty		= 0;
	knight->slow_count		= 0;
	knight->coord->x		= x;
	knight->coord->y		= y;
	knight->move_to->x		= -1;
	knight->move_to->y		= -1;
	knight->base->x			= -1;
	knight->base->y			= -1;
	knight->clock			= clk;
	knight->c_counter		= 0;
	knight->range			= randomIntGenerator ( 6, 10 );
	knight->patrol			= true;
	knight->acquired		= false;
	knight->blocked			= false;
	knight->follow			= false;
	knight->retreat			= false;
	knight->moving			= false;
	knight->print			= print;
	knight->follow_behav	= follow;
	knight->patrol_behav	= knight_patrol;
	knight->minPath			= a_star;
	knight->key_taken		= key_taken;
	knight->key_not_taken	= key_not_taken;
	return knight;
}

/**
 * verifica la distanza tra 2 oggetti
 * @param a		oggetto
 * @param b		oggetto
 * @param dist	distanza
 * @return		vero se i due oggetti sono nel range richiesto
 */
bool get_collision_coord ( VCOORD *a, VCOORD *b, int dist ) {
	assert ( a );
	assert ( b );

	if ( abs ( a->x - b->x ) <= dist && abs ( a->y - b->y ) <= ( dist/2 ) ) {
		return true;
	}

	return false;
}

/**
 * pulisce il livello corrente cancellando tutte le entità create
 * @param level
 */
void clear_level ( LEVEL *level ) {
	KNIGHT *knight = NULL;
	while ( ( knight = getFront ( level->knight_list ) ) != NULL ) {
		level->knight_list = dequeue ( level->knight_list );
		free ( knight->base );
		free ( knight->coord );
		free ( knight->move_to );
		clearSet ( knight->path->coord );
		free ( knight->path );
		free ( knight );

	}
	int i;
	for ( i = 0; i < level->graph->height; i++ ) {
		free ( level->graph->maze[i] );
	}
	free ( level->graph->maze );
	clearSet ( level->key_list );
	clearSet ( level->trap_list );
	clearSet ( level->bonus_list );
	clearSet ( level->player->weapon_list );
	level->player->weapon_list = NULL;

}