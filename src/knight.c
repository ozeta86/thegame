#include <unistd.h>
#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <sys/time.h>
#include <ncurses.h>
#include <assert.h>
#include <menu.h>
#include <form.h>
#include "./headers/structures.h"
#include "./headers/game.h"
#include "./headers/functions.h"
#include "./headers/knight.h"

#include "./headers/parser.h"
#include "./headers/pathfind.h"
#include "headers/lib.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

#define YELLOWONBLUE 	1
#define BLACKONWHITE 	2
#define WHITEONBLACK 	3
#define REDONBLACK   	4
#define WHITEONBLUE   	5
#define BLUEONYELLOW   	6
#define GREENONBLACK   	7
#define MAGENTAONBLACK	8
#define CYANONBLACK		9
#define BLACKONCYAN		10
#define BLACKONRED		11
#define ATTRIBS 		WA_BOLD

#define MARGINUP		4
#define SIDEMARGIN		4
#define NORTH(y) 		y - 1
#define SOUTH(y) 		y + 1
#define EAST(y) 		y + 1
#define WEST(y) 		y - 1

#define BYTES			128
#define MS_PER_UPDATE	20
#define KNIGHTHALT		200

//numero di facce del dado di gioco
#define DICEMAX			10
//costo stamina minimo
#define STAMINA_COST	10
//numero massimo di armi su livello
#define MAXWEAPONS		4



/**
 * stampa a schermo del cavaliere
 * @param win		oggetoto finestra
 * @param knight	oggetto cavaliere
 * @param cam		oggetto telecamera, usato per calcolare l'offset della mappa
 */
void knight_print ( WINDOW *win, KNIGHT *knight, CAMERA *cam ) {
	int COLOR = 0;
	//rileva lo stato del cavaliere e ne cambia il colore
	if ( knight->patrol == true ) {
		COLOR = MAGENTAONBLACK;
	} else if ( knight->follow == true ) {
		COLOR = CYANONBLACK;
	}
	
	if (  knight->health <= 0  ) {
		COLOR = REDONBLACK;
	}
	
	wattron ( win, COLOR_PAIR ( COLOR | ATTRIBS ) );
	mvwprintw ( win, knight->coord->y - cam->vOffset , knight->coord->x - cam->hOffset, knight->character );
	wattroff ( win, COLOR_PAIR ( COLOR | ATTRIBS ) );
}
/**
 * comportamento del cavaliere: pattugliamento
 * @param win				finestra
 * @param level				livello
 * @param knight			cavaliere
 * @param player			giocatore
 * @param size				dimensione area di destinazione
 * @param alg				puntatore a funzione di ricerca percorso minimo
 * @return 
 */


/**
 * comportamento cavaliere: inseguimento
 * @param win
 * @param level
 * @param knight
 * @param player
 * @param alg			CALLBACK: algoritmo di ricerca percorso minimo
 */
void knight_follow ( WINDOW *win, LEVEL *level, KNIGHT *knight, PLAYER *player, PATHD alg ) {
	//se il giocatore si sposta dall'ultima posizione conosciuta
	//ricalcolo la destinazione
	if ( player->coord->x != knight->move_to->x || player->coord->y != knight->move_to->y ) {
		knight->move_to->x = player->coord->x;
		knight->move_to->y = player->coord->y;
		//svuotamento vecchio percorso
		empty_path( knight->path );
		//creazione nuovo percorso ( se esistente )
		knight->path->coord = pathfind ( level->graph, knight->coord, knight->move_to, alg );
		//estrazione prima posizione del percorso
		free ( getFront ( knight->path->coord ) );
		knight->path->coord = dequeue ( knight->path->coord );
	}
	//spostamento cavaliere
	if ( knight->path->coord != NULL ) {
		move_knight ( knight );
	}
}
/**
 * cambio dello stato del cavaliere: lo stato viene impiegato, ad esempio, per cambiare 
 * il colore del cavaliere sullo schermo.
 * @param knight
 * @param follow
 * @param patrol
 * @param engage
 */

bool if_near_engage ( WINDOW *win, LEVEL *level, PLAYER *player, KNIGHT *knight, VCOORD *target ) {
	
	bool res = false;
	if ( get_collision_coord ( player->coord, target, knight->range ) ) {
		knight->follow_behav ( win, level, knight, player, knight->minPath );
		//cambio stato cavaliere
		if ( knight->engage == false ) {
			set_knight_state ( knight, true ,false, true );
		}
		res = true;
	}
	

	
	return res;
}
void set_knight_state ( KNIGHT *knight, bool follow, bool patrol, bool engage ){
	knight->follow = follow;
	knight->patrol = patrol;
	knight->engage = engage;
}
/**
 * comportamento del cavaliere quando la chiave non è stata presa
 * @param win
 * @param level
 * @param player
 * @param knight
 */
void key_not_taken ( WINDOW *win, LEVEL* level, PLAYER *player, KNIGHT *knight) {
	//GIOCATORE NEL RANGE DEL CAVALIERE: INSEGUIMENTO
	if ( get_collision_coord ( player->coord, knight->coord, knight->range ) ) {
		knight->follow_behav ( win, level, knight, player, knight->minPath );
		//cambio stato cavaliere
		if ( knight->follow == false ) {
			set_knight_state ( knight, true, false, true );
		}
		//GIOCATORE NEL RANGE DELLA BASE, CAVALIERE TORNA ALLA BASE E INSEGUE
	} else if ( get_collision_coord ( player->coord, knight->base, 2 ) ) {
		knight->follow_behav ( win, level, knight, player, knight->minPath );
		if ( knight->follow == false ) {
			set_knight_state ( knight, true, false, true );
		}
		//GIOCATORE FUORI DAL RANGE DELLA BASE, CAVALIERE PATTUGLIA
	} else {
		knight->patrol_behav ( win, level, knight, player, knight->range,  knight->minPath );
		if ( knight->patrol == false ) {
			set_knight_state ( knight, false, true, false );

		}
	}
	//CAVALIERE RAGGIUNGE GIOCATORE: ENTRA NEL COMBATTIMENTO
	if ( get_collision_coord ( player->coord, knight->coord, 0 ) ) {
		GAME_PAUSE = true;
		battle_window ( level, player, knight );
		knight->slow_count = KNIGHTHALT;
		knight->engage = false;

	}
}

/**
 * comportamento del cavaliere quando la chiave è stata presa
 * @param win
 * @param level
 * @param player
 * @param knight
 */
void key_taken ( WINDOW *win, LEVEL* level, PLAYER *player, KNIGHT *knight ) {
	//GIOCATORE NEL RANGE DEL CAVALIERE: INSEGUIMENTO
	if ( get_collision_coord ( player->coord, knight->coord, knight->range ) ) {
		knight->follow_behav ( win, level, knight, player, knight->minPath );
		if ( knight->follow == false ) {
			set_knight_state ( knight, true, false, true );

		}
	} else {
		//CAVALIERE PATTUGLIA
		knight->patrol_behav ( win, level, knight, player, knight->range, knight->minPath );
		if ( knight->patrol == false ) {
			knight->follow = false;
			set_knight_state ( knight, false, true, false );
		}
	}
	//CAVALIERE RAGGIUNGE GIOCATORE: ENTRA NEL COMBATTIMENTO
	if ( get_collision_coord ( player->coord, knight->coord, 0 ) ) {
		GAME_PAUSE = true;
		battle_window ( level, player, knight );
		knight->slow_count = KNIGHTHALT;
		knight->engage = false;
	}
}
/**
 * aggiorna lo stato del cavaliere
 * @param win
 * @param level
 * @param knight
 * @param player
 * @param alg	CALLBACK: algoritmo di ricerca percorso minimo
 */
void knight_update ( WINDOW *win, LEVEL *level, KNIGHT *knight, PLAYER *player, PATHD alg ) {
	
	//IMPOSTA IL TIMER PER COLPO DI RITARDO
	if ( knight->blocked == true && get_collision_coord ( player->coord, knight->coord, 16 ) ) {
		knight->slow_count = 50 - 5 * knight->difficulty;
		knight->blocked = false;
	} 

	//MOVIMENTO CAVALIERE
	//il cavaliere è immobilizzato
	if ( knight->slow_count > 0 ) {
		knight->slow_count--;
	//altrimenti può elaborare il comportamento:
	//se la chiave non è stata presa
	} else if ( knight->own_key->taken == false ) {
		knight->key_not_taken ( win, level, player, knight );
		//key_not_taken ( win, level, player, knight );
	//se la chiave è stata presa
	} else {
		knight->key_taken ( win, level, player, knight );
		//key_taken ( win, level, player, knight );
		
	}


}
/*
void knight_update ( WINDOW *win, LEVEL *level, KNIGHT *knight, PLAYER *player, PATHD alg ) {


	//IMPOSTA IL TIMER PER COLPO DI RITARDO
	if ( knight->blocked == true && get_collision_coord ( player->coord, knight->coord, 16 ) ) {
		knight->slow_count = 50 - 5 * knight->difficulty;
		knight->blocked = false;
	} 

	//MOVIMENTO CAVALIERE
	//il cavaliere è immobilizzato
	if ( knight->slow_count > 0 ) {
		knight->slow_count--;
	//altrimenti può elaborare il comportamento:
	//se la chiave non è stata presa
	} else if ( knight->own_key->taken == false ) {
		knight->key_not_taken ( win, level, player, knight );
	//se la chiave è stata presa
	} else {
		knight->key_taken ( win, level, player, knight );
	}

	if ( knight->path->coord != NULL ) {
		//elaboro la posizione successiva
		move_knight ( knight );
	} 

}
*/

/**
 * sposta un cavaliere di una casella su un percorso
 * @param knight	oggetto cavaliere
 * @param path		oggetto percorso
 */
void move_knight ( KNIGHT *knight ) {
	assert ( knight );
	PATH *path = knight->path;
	//simula la velocità del cavaliere
	if ( knight->c_counter > 0 ) {
		knight->c_counter -= knight->speed;
	} else {
		knight->c_counter 	= knight->clock;
		//estrae la posizione successiva
		VCOORD *tmp 		= ( VCOORD * ) getFront ( path->coord );
		path->coord 		= dequeue ( path->coord );
		if ( tmp != NULL ) {
			knight->coord->x 	= tmp->x;
			knight->coord->y 	= tmp->y;
			free ( tmp );
		} else {
			knight->moving = false;
		}
	}
}


int knight_patrol ( WINDOW *win, LEVEL *level, KNIGHT *knight, PLAYER *player, int size, PATHD alg ) {
	srand ( time ( NULL ) );
	
	// controllo stato del cavaliere
	if ( knight->patrol == true ) {

		//il cavaliere non è impegnato in un percorso
		if ( knight->path->coord == NULL ) {
			VCOORD *center = knight->base;

			VCOORD dest;

			do {
				dest = get_random_coord ( level, knight->base, size );
			} while ( level->graph->maze[dest.y][dest.x].k == 9 );

			knight->move_to->x = dest.x;
			knight->move_to->y = dest.y;
			//elaboro il nuovo percorso
			knight->path->coord = pathfind ( level->graph, knight->coord, knight->move_to, alg );
			//il cavaliere si sta già spostando
		} else {
			//elaboro la posizione successiva
			move_knight ( knight );
		}
	}


	return 0;
}
