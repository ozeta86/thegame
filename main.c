#include <ncurses.h>
#include <stdlib.h>
#include <time.h>
#include <string.h> 
#include "./src/headers/game.h"
#include "./src/headers/structures.h"
#include "./src/headers/lib.h"

//rileva il numero di mappe nella cartella maps
int get_levels_number ( char **levels );
//legge gli oggetti, genera la mappa e alloca la memoria
LEVEL **build_game ( int level_number, char **levels, int difficulty );
//menu principale di gioco
LEVEL ** game_start ( int level_number, int difficulty );
//controlla che le mappe del vettore levels siano presenti nella cartella maps
bool areMapsOk ( char **levels );
/*
array di strighe con sringa sentinella "NULL"

*/
char *messageArray[] = {
	"",
	"Premi [W],[S],[INVIO] per navigare nel menu.",
	"NULL"
};
//guida comandi
char *helpArray0[] = {
	"Premi [W],[A],[S],[D] per spostarti ",
	"nelle 4 direzioni.",
	"",
	"Premi [Spazio] quando sei vicino ad un nemico",
	"per fermarlo temporaneamente.", "",
	"Per saltare il livello premi [F1].",
	"NULL"
};
//titoli di coda
char *endTitles[] = {
	"Laboratorio di Algoritmi",
	"e strutture dati 2014",
	"",
	"NULL"
};
//array delle mappe
char *levels[] = {
	"./maps/implicit0.txt",
	"./maps/implicit1.txt",
	"./maps/implicit2.txt",
	"./maps/implicit3.txt",
	"./maps/implicit4.txt",
	"NULL"
};

int main ( int argc, char *argv[] ) {
	
	//inizializzazioni
	srand ( time ( NULL ) );

	int next_x = 0;
	int direction = 1;
	int i = 0;

	//inizializza mouse;
	mousemask ( ALL_MOUSE_EVENTS, NULL );

	//controlla che tutte le mappe siano presenti nella directory
	if ( areMapsOk ( levels ) == false ) {
		endwin();
		printf ("\nImpossibile trovare il file mappa, controllare il percorso.\n\nEsecuzione annullata.\n\n");
		exit ( -1 );
	}
	//estraggo il numero di mappe dall'array
	int level_number = get_levels_number ( levels );
	//inizializza modalità e opzioni ncurses
	inizialize_screen ( );
	int start_difficulty = 2;
	//carica il gioco
	LEVEL **level = game_start ( level_number, start_difficulty );

	//fine del gioco: completa la procedura di cancellazione dalla memoria
	for ( i = 0; level != NULL && i < level_number; i++ ) {
		free ( level[i] );
	}
	//reimposta il timeout di input della tastiera
	timeout ( -30 );
	endwin ( );
}
//controlla che i file nell'array levels siano presenti e leggibili nella directory
bool areMapsOk ( char **levels ) {
	bool test = true;
	FILE * pFile;
	int i = 0;
	while ( strcmp ( levels[i], "NULL" ) != 0 && test == true ) {
		if ( ( pFile = openStream ( levels[i], "r" ) ) != NULL ) {
			closeStream ( pFile );
		} else {
			test = false;
		}
		i++;
	}	
	
	return test;
}

//alloca memoria per i livelli e inizializza tutte le entità presenti
LEVEL **build_game ( int levels_number, char **levels, int difficulty ) {
	int i;
	LEVEL **level = ( LEVEL ** ) malloc ( levels_number * sizeof ( LEVEL ) );
	PLAYER *player = init_player ( "o", 100, 2, NULL, -1, -1, print_player );

	for ( i = 0; i < levels_number; i++ ) {

		level[i] = initialize_level ( level[i], player, difficulty, levels[i], knight_follow );
		difficulty++;
	}

	level[0]->count = levels_number;
	return level;
}

//restituisce il numero di livelli presenti nell'array levels
int get_levels_number ( char **levels ) {
	int i = 0;
	while ( strcmp ( levels[i], "NULL" ) != 0 ) {
		i++;
	}
	return i;
}

//mostra il menu di gioco e lo avvia nelle 3 modalità presenti. o esce dal gioco
LEVEL **game_start ( int level_number, int difficulty ) {


	LEVEL **level = NULL;
	//dialogNPanel ( "benvenuto nel Gioco", messageArray );
	int max_y, max_x, y, x;
	getmaxyx ( stdscr, max_y, max_x );

	x = max_x / 2;
	y = max_y / 2;
	//apre il menu di scelta di gioco
	int res;
	int game_status;
	while ( res != 3 ) {
		res = game_menu ( level, x, y );
		switch ( res ) {
				//new game -> easy
			case 0:
				//dialogNPanel ( "Guida", helpArray0 );
				level = build_game ( level_number, levels, difficulty );
				game_status = new_game ( level, 0, difficulty );
				if ( res == 1 ) {
					dialog_spanel ( "Hai vinto il gioco facile!" );
				}
				endwin ( );
				break;
				//new game -> medium
			case 1:
				difficulty = difficulty * 4;
				level = build_game ( level_number, levels, difficulty );
				game_status = new_game ( level, 0, difficulty );
				if ( res == 1 ) {
					dialog_spanel ( "Hai vinto il gioco intermedio!" );
				}				
				endwin ( );
				break;
				//new game -> hard
			case 2:
				difficulty = difficulty * 8;
				level = build_game ( level_number, levels, difficulty );
				game_status = new_game ( level, 0, difficulty );
				if ( res == 1 ) {
					dialog_spanel ( "Hai vinto il gioco difficile!" );
				}				
				endwin ( );
				break;

				//exit
			case 3:
				break;
			default:
				break;
		}
	}
	dialog_npanel ( "The Game", endTitles );
	return level;
}

